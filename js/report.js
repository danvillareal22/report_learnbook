



$(document).ready(function() {

$("#report_learnbook_report_activities").select2({
		placeholder: "Select Activities",
    	ajax: {
	    url: "/report/learnbook/ajax_activityselection.php",
	    dataType: 'json',
	    data: function (params) {
	      return {
	      	q: params.term, // search term
	        courseids: $('#learnbook_report_courses').val(), // search term
	        // page: params.page
	      };
	    },
	    processResults: function (data, page) {
	      return {
	        results: data.items
	      };
	    },
	    cache: true,
	  },
	  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    });
	window.pushState = function() {

		string = $('#learnbook_report').serialize();

		if (history.pushState) {
		    var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + string;
		    window.history.pushState({path:newurl},'',newurl);
		}
		
		try {
			// window.learnbookdatatable.draw();
		} catch (e) {

		}

	}

	$('input[name="daterange"]').daterangepicker({
		ranges: {
	       'Today': [moment(), moment()],
	       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	       'This Month': [moment().startOf('month'), moment().endOf('month')],
	       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	    },
	    opens : 'right',
	    locale: {
	      format: 'DD/MM/YYYY'
	    }
	});

	window.learnbookdatatable = $('#report_learnbook_reportserverside')
	.DataTable({
		"processing": true,
        "serverSide": true,
        "ordering": false,
        "bFilter": true,
        "oSearch": {"sSearch": $('#report_learnbook_search_type').val()},
        "dom": '<"top"flipT<"clear">>rt<"bottom"flip<"clear">>',
        "pagingType" : "simple",
        "pageLength" : 25,
        "lengthMenu": [[25, 50, 100, 250, 500, 1000], [25, 50, 100, 250, 500, 1000]],
        "tableTools" : {
        	"aButtons": [
	            {
	                "sExtends": "xls",
	                "sTitle": "Training Report Extract",
	                "sButtonText" : "Export Table"
	            }
	        ],
	        "sSwfPath": "copy_csv_xls_pdf.swf"
        },
        "ajax": {
            "url": "/report/learnbook/serverside_processing.php",
            "data": function ( d ) {
            	d.courses = $('#learnbook_report_courses').val();
            	d.cohorts = $('#learnbook_report_cohorts').val();
            	d.report_learnbook_report_activities = $('#report_learnbook_report_activities').val();
            	d.queryBuilderState = $('input[name=queryBuilderState]').val();
            	d.excludesuspended = + $('#learnbook_report_excludesuspended').is(':checked');
            	d.includeCourseGrades = + $('#learnbook_report_includeCourseGrades').is(':checked');
            	d.daterange = $('input[name="daterange"]').val();
            	d.status = $('#learnbook_report_status').val();
            }
        }
	});
	$("div.top").append('<div style="float: right; margin-right:5px;"><button name="SN" id="SN" value="Send Notification" class="DTTT_button" >Send Notification</button></div>');
	var dtable = $('#report_learnbook_reportserverside').dataTable().api();
	$(".dataTables_filter input")
	    .unbind() // Unbind previous default bindings
	    .bind("keypress", function(e) { // Bind our desired behavior
	        // If the length is 3 or more characters, or the user pressed ENTER, search
	        if(e.keyCode == 13) {
	            // Call the API search function

	            $('#report_learnbook_search_type').val($('.dataTables_filter input').val());
	            dtable.search($('.dataTables_filter input').val())
				window.pushState();

				$('#learnbook_report').submit();

	        }
	        // Ensure we clear the search if they backspace far enough
	        if(this.value == "") {
	            // dtable.search("").draw();
	        }
	        return;
	    });

	// $('.elevator').belowfold({delay:1000});

	var elementButton = document.querySelector('.elevator');

    var elevator = new Elevator({
        element: elementButton,
        duration: 2500
    });

    // var tableTools = new $.fn.dataTable.TableTools(window.learnbookdatatable, {
        
    // } );

    // $(tableTools.fnContainer()).insertAfter('#learnbook_report_datatables');

    $('#learnbook_createNotification').click(function(event) {
		event.preventDefault();
		if (!window.location.origin) {
		  window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
		}
		var loc = window.location.pathname;
		var dir = loc.substring(0, loc.lastIndexOf('/'));
		string = $('#learnbook_report').serialize();
		addr = window.location.origin + dir + '/testnotification.php?' + string
		window.location.href = addr;
		return false;
	});

	$('#learnbook_report_savedreports').submit(function(event) {

		event.preventDefault();

		if (!window.location.origin) {
		  window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
		}

		var loc = window.location.pathname;
		var dir = loc.substring(0, loc.lastIndexOf('/'));

		string = $('#learnbook_report').serialize();

		string += "&name=" + $('#learnbook_report_savedreports_reportname').val();
		string += "&schedule=" + $('#learnbook_report_savedreports_reportschedule').val();
		string += "&email=" + $('#learnbook_report_savedreports_email').val();

		addr = window.location.origin + dir + '/schedulereport.php?' + string

		window.location.href = addr;

		return false;

	});

	$('#learnbook_report').submit(function(event) {

		event.preventDefault();

		$('input[name=queryBuilderState]').val(JSON.stringify($('#builder').queryBuilder('getRules')));

		$('input[name=queryBuilderQuery]').val(JSON.stringify($('#builder').queryBuilder('getSQL', 'question_mark')))

		window.pushState();

		window.learnbookdatatable.draw();

		return false;


	});

	try {
		$('#runreport').click(function(event) {
			event.preventDefault();
			// $('.nav-tabs a[aria-controls="home"]').tab("show");
			$('#learnbook_report').submit();
			return false;
		});
	} catch (e) {

	}

	$('#learnbook_report_cohorts').select2({
		placeholder: "Select Cohorts",
	});

	// $('#learnbook_report_savedreports_reportschedule').select2({
	// 	placeholder: "When?",
	// });

	$('#learnbook_report_courses').select2({
		placeholder: "Select Courses",
	});

	

    qbopts = {
	    filters: window.advancedfilters,
	  	allow_empty : true,
	  	allow_groups : 3,
	  	optgroups : window.moodlequerybuilderoptgroups,
	  	rules : JSON.parse($('input[name=queryBuilderState]').val()),
	  	icons : {
	  		add_group : 'fa fa-plus-circle',
	  		add_rule : 'fa fa-plus-circle',
	  		remove_group : 'fa fa-minus-circle',
	  		remove_rule : 'fa fa-minus-circle',
	  		error : 'fa fa-exclamation-circle',
	  	}
	};

    try {
    	$('#builder').queryBuilder(qbopts);
    } catch (e) { // Ensure no bad objects prevent this plugin from booting.
    	qbopts.rules = {"condition":"AND","rules":[]};
    	$('#builder').queryBuilder(qbopts);
    }

    $('#displayadvanced').click(function(event) {
    	$('#builder').toggle();
    	$('#builderlabel').toggle();
    });


    // Javascript to enable link to tab
    //i cant believe this is what you have to do 


    if(window.location.hash != "") {
         $('a[href="' + window.location.hash + '"]').click();
     }

     


	// var str = window.location.href;
	// var match = str.match(/[^=&?]+\s*=\s*[^&#]*/g);
	// var obj = {};
	// for ( var i = match.length; i--; ) {
	//   var spl = match[i].split("=");
	//   var name = spl[0].replace("[]", "");
	//   var value = spl[1];

	//   obj[name] = obj[name] || [];
	//   obj[name].push(value);
	// }
$('#learnbook_report').trigger('change');
 //    $("#report_learnbook_report_activities").val(obj['report_learnbook_report_activities%5B%5D']).trigger('change');

 //sweetalert()
 $("#SN").click(function(){
	 swal.queue([{
	  title: 'Are you sure?',
	  text: "You are about to send email to notification on all users on the report.",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonText: 'Yes',
	  confirmButtonClass: 'btn btn-primary',
	  cancelButtonClass: 'btn btn-danger',
	  showLoaderOnConfirm: true,
	  preConfirm: function () {
		return new Promise(function (resolve) {
		  string = $('#learnbook_report').serialize();
          var newurl = window.location.protocol + "//" + window.location.host + '/report/learnbook/serverside_processing.php' + '?' + string +'&SN=true&length=0&start=0&draw=1';

			$.ajax({url: newurl, success: function(result){
				confirmButtonClass: 'btn btn-primary',
				swal.insertQueueStep(result)
				resolve()
			},
			error: function (error) {
				swal.insertQueueStep('Error on request!')
				resolve()
			}});
			
		})
	  }
	}]);
 });
 
});