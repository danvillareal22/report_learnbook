$(document).ready(function() {

	$('input.date').daterangepicker({
		// ranges: {
	 //       'Today': [moment(), moment()],
	 //       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	 //       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	 //       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	 //       'This Month': [moment().startOf('month'), moment().endOf('month')],
	 //       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	 //    },
	    opens : 'right',
	    locale: {
	      format: 'DD/MM/YYYY'
	    }
	});

	$('#learnbook_report_cohorts').select2({
		placeholder: "Select Cohorts",
	});

	$('#learnbook_report_courses').select2({
		placeholder: "Select Courses",
	});

	$("#report_learnbook_report_activities").select2({
		placeholder: "Select Activities",
    	ajax: {
	    url: "/report/learnbook/ajax_activityselection.php",
	    dataType: 'json',
	    data: function (params) {
	      return {
	      	q: params.term, // search term
	        courseids: $('#learnbook_report_courses').val(), // search term
	        // page: params.page
	      };
	    },
	    processResults: function (data, page) {
	      return {
	        results: data.items
	      };
	    },
	    cache: true,
	  },
	  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    });

    $('#learnbook_report').on('change', function() {

    	// d.courses = $('#learnbook_report_courses').val();
    	// d.cohorts = $('#learnbook_report_cohorts').val();

    	templates = {
    		"on_enrolment" : {
    			"status" : "-1",
    			"additional_conditions" : {
	    			"condition": "AND",
	    			"rules":[
	    				{
	    					"id":"%%enrolmenttimecreated%%",
	    					"field":"%%enrolmenttimecreated%%",
	    					"type":"date",
	    					"input":"text",
	    					"operator":"greater",
	    					"value": (new moment().format('YYYY/MM/DD'))
	    				}
	    			]
	    		}
    		},
    		"incompleteOneMonthAfterAccountCreation" : {
    			"status" : "notcomplete",
    			"additional_conditions" : {
					"condition":"AND",
					"rules":[
						{	"id":"%%U.timecreated_interval%%",
							"field":"%%U.timecreated_interval%%",
							"type":"string",
							"input":"select",
							"operator":"less",
							"value":"%%interval:-4 weeks%%"
						}
					]
				}
    		}
    	}

    	selectedTemplate = $('select[name=notificationtemplate]').val();

    	if (typeof templates[selectedTemplate] != 'undefined') {
    		console.log(templates[selectedTemplate]);
    		$('input[name=status],select[name=status]').val(templates[selectedTemplate].status);
    		$('input[name=queryBuilderState]').val(JSON.stringify(templates[selectedTemplate].additional_conditions))
        }
    	
    	formParams = $('#learnbook_report').serialize();

    	console.log(formParams);
    });

    $('#learnbook_report').trigger('change');


    






















});