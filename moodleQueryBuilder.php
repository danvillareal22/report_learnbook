<?php

class moodleQueryBuilder {

	public function __construct() {
		$this->params = array();
	}

	public function parseFilter($filter) {
		return $filter;
	}

	public function parseOperator($operator, $value, $id) {

		global $CFG, $DB;

		$allowedIDs = array(
			'U.username' => null,
			'U.firstname' => null,
			'U.lastname' => null,
			'U.email' => null,
			'U.idnumber' => null,
			'U.country' => null,
			'U.lang' => null,
			'U.confirmed' => null,
			'U.firstaccess' => null,
			'%%U.firstaccess_interval%%' => '(U.firstaccess)',
			'U.lastaccess' => null,
			'%%U.lastaccess_interval%%' => '(U.lastaccess)',
			'U.lastlogin' => null,
			'%%U.lastlogin_interval%%' => '(U.lastlogin)',
			'U.timecreated' => null,
			'%%U.timecreated_interval%%' => '(U.timecreated)',
			'U.timemodified' => null,
			'%%U.timemodified_interval%%' => '(U.timemodified)',
			'U.suspended' => null,
			'C.fullname' => null,
			'C.shortname' => null,
			'C.idnumber' => null,
			'C.format' => null,
			'C.showgrades' => null,
			'C.startdate' => null,
			'%%C.startdate_interval%%' => '(C.startdate)',
			'C.visible' => null,
			'C.lang' => null,
			'COC.name' => null,
			'GI.itemtype' => null,
			'GI.itemname' => null,
			'GI.itemmodule' => null,
			'GI.hidden' => null,
			'%%php:isaccessible%%' => null,
			'%%gradefinalgrade%%' => '(SELECT finalgrade FROM {grade_grades} GG WHERE GG.userid = U.id AND GG.itemid = GI.id)',
			'%%gradetimemodified%%' => '(SELECT timemodified FROM {grade_grades} GG WHERE GG.userid = U.id AND GG.itemid = GI.id)',
			'%%gradetimemodified_interval%%' => '(SELECT timemodified FROM {grade_grades} GG WHERE GG.userid = U.id AND GG.itemid = GI.id)',
			'%%enrolmenttimecreated%%' => '(SELECT (CASE UE.timestart WHEN 0 THEN UE.timecreated ELSE UE.timestart END) FROM {user_enrolments} UE, {enrol} E WHERE E.id = UE.enrolid AND UE.userid = U.id AND E.courseid = C.id LIMIT 1)',
			'%%enrolmenttimecreated_interval%%' => '(SELECT (CASE UE.timestart WHEN 0 THEN UE.timecreated ELSE UE.timestart END) FROM {user_enrolments} UE, {enrol} E WHERE E.id = UE.enrolid AND UE.userid = U.id AND E.courseid = C.id LIMIT 1)',
		);

		if (@$CFG->cpdhoursfield) {
			$allowedIDs['CM.cmhours'] = null;
		}

		$modifiers = array(
			'C.startdate' => 'strtotime',
			'U.firstaccess' => 'strtotime',
			'U.lastaccess' => 'strtotime',
			'U.lastlogin' => 'strtotime',
			'U.timecreated' => 'strtotime',
			'U.timemodified' => 'strtotime',
			'%%gradetimemodified%%' => 'strtotime',
			'%%enrolmenttimecreated%%' => 'strtotime',
		);

		$matches_interval = array();
		preg_match('/%%interval:(.+)%%/', $value, $matches_interval);

		if (!empty($matches_interval[1])) {
			$value = strtotime($matches_interval[1]);
		}

		if (isset($modifiers[$id])) {
			if ($modifiers[$id] == 'strtotime') {
				$value = strtotime($value);
			}
		}

		$userinfofields = $DB->get_records_sql("SELECT * FROM {user_info_field} WHERE datatype IN ('text')"); // Only Text until we test other cases...
		foreach ($userinfofields as $userinfofield) {
			$allowedIDs["%%profile_field_" . $userinfofield->shortname . "%%"] = "(SELECT UID.data FROM mdl_user_info_data UID WHERE UID.fieldid = " . $userinfofield->id . " AND UID.userid = U.id)";
		}

		if (!in_array($id, array_keys($allowedIDs))) {
			throw new Exception('Invalid Filter');
		}

		$operators = '{
	        "equal":            { "op": "= ?" },
	        "not_equal":        { "op": "!= ?" },
	        "in":               { "op": "IN(?)",          "sep": ", " },
	        "not_in":           { "op": "NOT IN(?)",      "sep": ", " },
	        "less":             { "op": "< ?" },
	        "less_or_equal":    { "op": "<= ?" },
	        "greater":          { "op": "> ?" },
	        "greater_or_equal": { "op": ">= ?" },
	        "between":          { "op": "BETWEEN ?",      "sep": " AND " },
	        "not_between":      { "op": "NOT BETWEEN ?",  "sep": " AND " },
	        "begins_with":      { "op": "LIKE(?)",        "mod": "{0}%" },
	        "not_begins_with":  { "op": "NOT LIKE(?)",    "mod": "{0}%" },
	        "contains":         { "op": "LIKE(?)",        "mod": "%{0}%" },
	        "not_contains":     { "op": "NOT LIKE(?)",    "mod": "%{0}%" },
	        "ends_with":        { "op": "LIKE(?)",        "mod": "%{0}" },
	        "not_ends_with":    { "op": "NOT LIKE(?)",    "mod": "%{0}" },
	        "is_empty":         { "op": "= \"\"" },
	        "is_not_empty":     { "op": "!= \"\"" },
	        "is_null":          { "op": "IS NULL" },
	        "is_not_null":      { "op": "IS NOT NULL" }
	    }';


		$operators = json_decode($operators);
		$validOperator = false;
		$op = '';
		foreach ($operators as $opkey => $opvalue) {
			if ($opkey != $operator) {
				continue;
			}

			if (!is_null($value)) {
				if (isset($opvalue->mod)) {
					$value = str_replace('{0}', $value, $opvalue->mod);
					$this->params[] = $value;
				} else {
					$this->params[] = $value;
				}
			}

			$validOperator = true;
			$filter = $id;
			if (!is_null($allowedIDs[$id])) {
				$filter = $allowedIDs[$id];
			}
			$op = '(' . $filter . ') ' . $opvalue->op;
			break;
		}

		if (!$validOperator) {
			throw new Exception ('Invalid Operator');
		}
		
		return $op;
	}

	public function parseState($input, $query = "") {
		$count = count($input->rules);
		foreach ($input->rules as $key => $rule) {

			if ($rule->id == '%%php:isaccessible%%') {
				continue;
			}

			if (isset($rule->rules)) {
				$query .= "(";
				$query .= self::parseState($rule);
				$query .= ")";
			} else {
				// self::parseValue($rule->value, $rule->id);
				$query .= self::parseOperator($rule->operator, $rule->value, $rule->id);
				if (($key) < ($count -1)) {
					$query .= " " . $input->condition;
				}
			}
		}

		return $query;
	}
	
	public static function renderJavascriptVariables() {

		ob_start();

		echo "<script>";
			echo "window.advancedfilters = " . json_encode(moodleQueryBuilder::generateOptions()) . ";";
			echo "window.moodlequerybuilderoptgroups = " . json_encode(moodleQueryBuilder::generateOptGroups()) . ";";
		echo "</script>";

		return ob_get_clean();

	}

	// public static function parseQuery($query) {

	// 	global $DB;

	// 	$placeholders = array();

	// 	$userinfofields = $DB->get_records_sql("SELECT * FROM {user_info_field} WHERE datatype IN ('text')"); // Only Text until we test other cases...
	// 	foreach ($userinfofields as $userinfofield) {
	// 		$placeholders["%%profile_field_" . $userinfofield->shortname . "%%"] = "(SELECT UID.data FROM mdl_user_info_data UID WHERE UID.fieldid = " . $userinfofield->id . " AND UID.userid = U.id)";
	// 	}

	// 	$placeholders["%%gradefinalgrade%%"] = "(SELECT finalgrade FROM {grade_grades} GG WHERE GG.userid = U.id AND GG.itemid = GI.id)";

	// 	foreach ($placeholders as $placeholder => $value) {
	// 		$query = str_replace($placeholder, $value, $query);
	// 	}

	// 	return $query;
	// }

	public static function generateOptGroups() {

		$optgroups = array(
			"learner" => "Learner",
			"course" => "Course",
			"coursecategory" => "Course Category",
			"enrolment" => "Enrolment",
			"gradeitem" => "Grade Item",
		);

		return $optgroups;
	}

	public static function generateOptions() {

		global $DB, $CFG;

		$querybuilder = array();

		$querybuilder[] = array(
			"id" => "U.username",
			"label" => "Username",
			"type" => "string",
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "U.firstname",
			"label" => "First Name",
			"type" => "string",
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "U.lastname",
			"label" => "Last Name",
			"type" => "string",
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "U.email",
			"label" => "Email",
			"type" => "string",
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "U.idnumber",
			"label" => "ID Number",
			"type" => "string",
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "U.country",
			"label" => "Country",
			"type" => "string",
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "U.lang",
			"label" => "Language",
			"type" => "string",
			"optgroup" => "learner",
		);

		$userinfofields = $DB->get_records_sql("SELECT * FROM {user_info_field} WHERE datatype IN ('text')"); // Only Text until we test other cases...

		foreach ($userinfofields as $userinfofield) {
			$querybuilder[] = array(
				"id" => "%%profile_field_" . $userinfofield->shortname . "%%",
				"label" => $userinfofield->name,
				"type" => "string",
				"optgroup" => "learner",
			);
		}

		$querybuilder[] = array(
			"id" => "U.firstaccess",
			"label" => "First Access",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "%%U.firstaccess_interval%%",
			"label" => "First Access (Interval)",
			"type" => "string",
			"input" => "select",
			"values" => array(
				'%%interval:first day of this month%%' => 'Start of this Month',
				'%%interval:first day of next month%%' => 'End of this Month',
				'%%interval:-4 weeks%%' => 'Four Weeks Ago',
				'%%interval:-7 days%%' => 'Last 7 Days',
				'%%interval:yesterday%%' => 'Yesterday',
				'%%interval:today%%' => 'Today'
			),
			"optgroup" => "learner",
			"operators" => array("equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal")
		);

		$querybuilder[] = array(
			"id" => "U.lastaccess",
			"label" => "Last Access",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "%%U.lastaccess_interval%%",
			"label" => "Last Access (Interval)",
			"type" => "string",
			"input" => "select",
			"values" => array(
				'%%interval:first day of this month%%' => 'Start of this Month',
				'%%interval:first day of next month%%' => 'End of this Month',
				'%%interval:-4 weeks%%' => 'Four Weeks Ago',
				'%%interval:-7 days%%' => 'Last 7 Days',
				'%%interval:yesterday%%' => 'Yesterday',
				'%%interval:today%%' => 'Today'
			),
			"optgroup" => "learner",
			"operators" => array("equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal")
		);

		$querybuilder[] = array(
			"id" => "U.lastlogin",
			"label" => "Last Login",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "%%U.lastlogin_interval%%",
			"label" => "Last Login (Interval)",
			"type" => "string",
			"input" => "select",
			"values" => array(
				'%%interval:first day of this month%%' => 'Start of this Month',
				'%%interval:first day of next month%%' => 'End of this Month',
				'%%interval:-4 weeks%%' => 'Four Weeks Ago',
				'%%interval:-7 days%%' => 'Last 7 Days',
				'%%interval:yesterday%%' => 'Yesterday',
				'%%interval:today%%' => 'Today'
			),
			"optgroup" => "learner",
			"operators" => array("equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal")
		);

		$querybuilder[] = array(
			"id" => "U.timecreated",
			"label" => "Created",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "%%U.timecreated_interval%%",
			"label" => "Created (Interval)",
			"type" => "string",
			"input" => "select",
			"values" => array(
				'%%interval:first day of this month%%' => 'Start of this Month',
				'%%interval:first day of next month%%' => 'End of this Month',
				'%%interval:-4 weeks%%' => 'Four Weeks Ago',
				'%%interval:-7 days%%' => 'Last 7 Days',
				'%%interval:yesterday%%' => 'Yesterday',
				'%%interval:today%%' => 'Today'
			),
			"optgroup" => "learner",
			"operators" => array("equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal")
		);

		$querybuilder[] = array(
			"id" => "U.timemodified",
			"label" => "Modified",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "learner",
		);

		$querybuilder[] = array(
			"id" => "%%U.timemodified_interval%%",
			"label" => "Modified (Interval)",
			"type" => "string",
			"input" => "select",
			"values" => array(
				'%%interval:first day of this month%%' => 'Start of this Month',
				'%%interval:first day of next month%%' => 'End of this Month',
				'%%interval:-4 weeks%%' => 'Four Weeks Ago',
				'%%interval:-7 days%%' => 'Last 7 Days',
				'%%interval:yesterday%%' => 'Yesterday',
				'%%interval:today%%' => 'Today'
			),
			"optgroup" => "learner",
			"operators" => array("equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal")
		);

		$querybuilder[] = array(
			"id" => "U.confirmed",
			"label" => "Confirmed",
			"type" => "integer",
			"input" => "radio",
			"values" => array(
				"0" => "No",
				"1" => "Yes"
			),
			"optgroup" => "learner",
			"operators" => array("equal")
		);

		$querybuilder[] = array(
			"id" => "U.suspended",
			"label" => "Suspended",
			"type" => "integer",
			"input" => "radio",
			"values" => array(
				"0" => "No",
				"1" => "Yes"
			),
			"optgroup" => "learner",
			"operators" => array("equal")
		);

		$querybuilder[] = array(
			"id" => "C.fullname",
			"label" => "Course Name",
			"type" => "string",
			"optgroup" => "course",
		);

		$querybuilder[] = array(
			"id" => "C.shortname",
			"label" => "Course Shortname",
			"type" => "string",
			"optgroup" => "course",
		);

		$querybuilder[] = array(
			"id" => "C.idnumber",
			"label" => "Course ID Number",
			"type" => "string",
			"optgroup" => "course",
		);

		$availableFormats = $DB->get_records_sql("SELECT DISTINCT format FROM {course} WHERE format NOT IN ('site')");
		$formats = array();
		foreach ($availableFormats as $format) {
			$formats[] = $format;
		}
		$querybuilder[] = array(
			"id" => "C.format",
			"label" => "Course Format",
			"type" => "string",
			"input" => "select",
			"values" => $formats,
			"optgroup" => "course",
			"operators" => array("equal", "not_equal")
		);

		$querybuilder[] = array(
			"id" => "C.showgrades",
			"label" => "Show Gradebook",
			"type" => "integer",
			"input" => "radio",
			"values" => array(
				"0" => "No",
				"1" => "Yes"
			),
			"optgroup" => "course",
			"operators" => array("equal")
		);

		$querybuilder[] = array(
			"id" => "C.startdate",
			"label" => "Start Date",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "course",
		);

		$querybuilder[] = array(
			"id" => "C.visible",
			"label" => "Visible",
			"type" => "integer",
			"input" => "radio",
			"values" => array(
				"0" => "Hide",
				"1" => "Show"
			),
			"optgroup" => "course",
			"operators" => array("equal")
		);

		$querybuilder[] = array(
			"id" => "C.lang",
			"label" => "Language",
			"type" => "string",
			"optgroup" => "course",
		);

		$querybuilder[] = array(
			"id" => "C.timecreated",
			"label" => "Created",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "course",
		);

		$querybuilder[] = array(
			"id" => "C.timemodified",
			"label" => "Modified",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "course",
		);

		$querybuilder[] = array(
			"id" => "COC.name",
			"label" => "Course Category Name",
			"type" => "string",
			"optgroup" => "coursecategory",
		);

		$querybuilder[] = array(
			"id" => "GI.itemname",
			"label" => "Activity Name",
			"type" => "string",
			"optgroup" => "gradeitem",
		);

		$availablemodules = $DB->get_records_sql("SELECT id, name FROM {modules}");
		$modules = array();
		foreach ($availablemodules as $key => $module) {
			$modules[$module->name] = $module->name;
		}
		$querybuilder[] = array(
			"id" => "GI.itemmodule",
			"label" => "Activity Module",
			"type" => "string",
			"input" => "select",
			"values" => $modules,
			"optgroup" => "gradeitem",
			"operators" => array("equal", "not_equal")
		);

		$querybuilder[] = array(
			"id" => "GI.itemtype",
			"label" => "Activity Type",
			"type" => "string",
			"input" => "select",
			"values" => array('course', 'manual', 'mod'),
			"optgroup" => "gradeitem",
			"operators" => array("equal", "not_equal")
		);

		// $querybuilder[] = array(
		// 	"id" => "%%gradeoverridden%%",
		// 	"label" => "Overridden",
		// 	"type" => "integer",
		// 	"input" => "radio",
		// 	"values" => array(
		// 		"0" => "No",
		// 		"1" => "Yes"
		// 	),
		// 	"optgroup" => "gradeitem",
		// 	"operators" => array("equal")
		// );

		$querybuilder[] = array(
			"id" => "%%gradefinalgrade%%",
			"label" => "Final Grade",
			"type" => "integer",
			"optgroup" => "gradeitem",
			"operators" => array("equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal")
		);

		$querybuilder[] = array(
			"id" => "%%gradetimemodified%%",
			"label" => "Date Graded",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "gradeitem",
		);

		$querybuilder[] = array(
			"id" => "%%enrolmenttimecreated%%",
			"label" => "Enrolment Start",
			"type" => "date",
			"validation" => array(
				"format" => "YYYY/MM/DD"
			),
			"plugin" => "datepicker",
			"plugin_config" => array(
				"format" => "yyyy/mm/dd",
				"todayBtn" => "linked",
				"todayHighlight" => true,
				"autclose" => true
			),
			"optgroup" => "enrolment",
		);

		$querybuilder[] = array(
			"id" => "%%enrolmenttimecreated_interval%%",
			"label" => "Enrolment Start (Interval)",
			"type" => "string",
			"input" => "select",
			"values" => array(
				'%%interval:first day of this month%%' => 'Start of this Month',
				'%%interval:first day of next month%%' => 'End of this Month',
				'%%interval:-4 weeks%%' => 'Four Weeks Ago',
				'%%interval:-7 days%%' => 'Last 7 Days',
				'%%interval:yesterday%%' => 'Yesterday',
				'%%interval:today%%' => 'Today',
				'%%interval:tomorrow%%' => 'Tomorrow'
			),
			"optgroup" => "enrolment",
			"operators" => array("equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal")
		);

		$querybuilder[] = array(
			"id" => "%%gradetimemodified_interval%%",
			"label" => "Date Graded (Interval)",
			"type" => "string",
			"input" => "select",
			"values" => array(
				'%%interval:first day of this month%%' => 'Start of this Month',
				'%%interval:first day of next month%%' => 'End of this Month',
				'%%interval:-4 weeks%%' => 'Four Weeks Ago',
				'%%interval:-7 days%%' => 'Last 7 Days',
				'%%interval:yesterday%%' => 'Yesterday',
				'%%interval:today%%' => 'Today'
			),
			"optgroup" => "gradeitem",
			"operators" => array("equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal")
		);

		$querybuilder[] = array(
			"id" => "GI.hidden",
			"label" => "Hidden",
			"type" => "integer",
			"input" => "radio",
			"values" => array(
				"1" => "Hidden",
				"0" => "Not Hidden"
			),
			"optgroup" => "gradeitem",
			"operators" => array("equal")
		);

		if (@$CFG->cpdhoursfield) {
			$querybuilder[] = array(
				"id" => "CM.cmhours",
				"label" => "CPD Hours",
				"type" => "string",
				"optgroup" => "gradeitem",
				"operators" => array("equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal")
			);
		}

		return $querybuilder;

	}

}