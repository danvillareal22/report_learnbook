<?php

$ADMIN->add('reports', new admin_externalpage('learnbook_report', 'Learnbook Report',
$CFG->wwwroot . "/report/learnbook/index.php", "moodle/role:manage"));

$settings = null;

if ($hassiteconfig) {

    $ADMIN->add('reports', $settings);
    
	$settings = new admin_settingpage(
		'report_learnbook',
		new lang_string('pluginname', 'report_learnbook')
	);

	$settings->add(new admin_setting_heading('report_learnbook', '', get_string('pluginname_desc', 'report_learnbook')));
	
	$name = 'report_learnbook/reportfields';
    $visiblename = get_string('reportfields','report_learnbook');
    $description = get_string('reportfields_desc','report_learnbook');

    $settings->add(new admin_setting_configtext($name, $visiblename,
                       $description, ''));

    // $settings->add(new admin_setting_text($name, $visiblename, $description, true));
    //include grade
    $name = 'report_learnbook/grade';
    $visiblename = get_string('grade','report_learnbook');
    $description = get_string('grade_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    //include date graded
    $name = 'report_learnbook/dategrade';
    $visiblename = get_string('dategrade','report_learnbook');
    $description = get_string('dategrade_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    

    $name = 'report_learnbook/disablerecurring';
    $visiblename = get_string('disablerecurring','report_learnbook');
    $description = get_string('disablerecurring_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    $name = 'report_learnbook/enablescheduling';
    $visiblename = get_string('enablescheduling','report_learnbook');
    $description = get_string('enablescheduling_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    $name = 'report_learnbook/enablelearnbooktranscript';
    $visiblename = get_string('enablelearnbooktranscript','report_learnbook');
    $description = get_string('enablelearnbooktranscript_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    $name = 'report_learnbook/enablecustomscheduling';
    $visiblename = get_string('enablecustomscheduling','report_learnbook');
    $description = get_string('enablecustomscheduling_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    $name = 'report_learnbook/enableactivitylevelfiltering';
    $visiblename = get_string('enableactivitylevelfiltering','report_learnbook');
    $description = get_string('enableactivitylevelfiltering_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    $name = 'report_learnbook/usefontawesome';
    $visiblename = get_string('usefontawesome','report_learnbook');
    $description = get_string('usefontawesome_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    $name = 'report_learnbook/userealcounts';
    $visiblename = get_string('userealcounts','report_learnbook');
    $description = get_string('userealcounts_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    $name = 'report_learnbook/enableadvancedfilters';
    $visiblename = get_string('enableadvancedfilters','report_learnbook');
    $description = get_string('enableadvancedfilters_desc','report_learnbook');

    $settings->add(new admin_setting_configcheckbox($name, $visiblename, $description, true));

    $name = 'report_learnbook/emailsubject';
    $visiblename = get_string('emailsubject','report_learnbook');
    $description = get_string('emailsubject_desc','report_learnbook');

    $settings->add(new admin_setting_configtext($name, $visiblename, $description, 'Learnbook Email Notification'));
    
    $link = $CFG->wwwroot . '/report/learnbook/downloadsavedreport.php?id=' . $notification->id;
    
    
    $message = "Hi {{firstname}} {{lastname}},\r\n\r\nYou are receiving this email because you have saved a report.\r\n\r\nThe report can be downloaded by going to Learnbook Report.\nThis is an automated message sent from the {{fullname}} LMS.";

    $name = 'report_learnbook/emailbody';
    $visiblename = get_string('emailbody','report_learnbook');
    $description = get_string('emailbody_desc','report_learnbook');

    $settings->add(new admin_setting_confightmleditor($name, $visiblename, $description, $message));
}

