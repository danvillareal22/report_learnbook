<?php

require_once('../../config.php');

require_login();

require_once($CFG->dirroot . '/report/learnbook/lib.php');
require_once($CFG->dirroot . '/report/learnbook/usermodel.php');

ob_start();
$cohorts = optional_param_array('cohorts', array(), PARAM_RAW);
$courses = optional_param_array('courses', array(), PARAM_RAW);
$activities = optional_param_array('report_learnbook_activities', array(), PARAM_RAW);
ob_get_clean();

$excludeSuspended = optional_param('excludesuspended', true ,PARAM_INT);
$includeCourseGrades = optional_param('includeCourseGrades', false ,PARAM_INT);

$daterange = optional_param('daterange', false, PARAM_RAW);
$daterange = explode('-', $daterange);

$startdate = 0;
$enddate = time();
if ($daterange) {
	$startdate = strtotime($daterange[0]);
	if (isset($daterange[1])) {
		$enddate = new DateTime();
		$enddate->setTimestamp(strtotime($daterange[1]));
		$enddate->modify('tomorrow');
		$enddate = $enddate->getTimestamp();
	} else {
		$enddate = new DateTime();
		$enddate->setTimestamp($startdate);
		$enddate->modify('tomorrow');
		$enddate = $enddate->getTimestamp();
	}
}

$options = array();
$options['startdate'] = $startdate;
$options['enddate'] = $enddate;
$options['includeCourseGrades'] = $includeCourseGrades;
$options['returnRecordSet'] = true;
$options['excludesuspended'] = $excludeSuspended;
$options['status'] = optional_param('status', -1,PARAM_RAW);
$options['limit'] = required_param('length', PARAM_INT);
$options['offset'] = required_param('start', PARAM_INT);

$optionsCount = array();
$optionsCount['includeCourseGrades'] = $options['includeCourseGrades'];
$optionsCount['returnTotalCount'] = true;
$optionsCount['excludesuspended'] = $excludeSuspended;
$optionsCount['status'] = $options['status'];
$optionsCount['startdate'] = $startdate;
$optionsCount['enddate'] = $enddate;

$showAllCohorts = in_array('-1', $cohorts);
$showAllCourses = in_array('-1', $courses);

if ($showAllCohorts) {
	$userids = array();
} else {
	$userids = report_learnbook_report::fetchUsersInCohorts($cohorts, $excludeSuspended);
}

if ($showAllCourses) {
	$courses = array();
}

$paramstring = $_SERVER['QUERY_STRING'];
$params = array();
parse_str($paramstring, $params);

$config = get_config('report_learnbook');

if (!$config->userealcounts) {
	$CFG->report_learnbook_skipcount = true;
} else {
	$CFG->report_learnbook_skipcount = false;
}

if (@$CFG->report_learnbook_skipcount == true) {

} else {

	$totalCount = report_learnbook_usermodel::fetchUsers($userids, $courses, $optionsCount, $params);
	$totalCount = reset($totalCount);

}

$paramsBulk = $params;
if($paramsBulk['SN'] == true){
	$options['limit'] = $totalCount->count;
}

$paramsBulk['updateBulkActionsSelection'] = true;

report_learnbook_usermodel::fetchUsers($userids, $courses, $options, $paramsBulk);

$recordset = report_learnbook_usermodel::fetchUsers($userids, $courses, $options, $params);

$headers = serverreport::fetchColumns();

$data = new stdClass();
$data->sEcho = required_param('draw', PARAM_INT);
$data->aaData = array();

$count = 0;

$emaildata = array();
$temparray = array();

foreach ($recordset as $record) {

	$row = array();

	/*

	Array
(
    [0] => ID
    [1] => Name
    [2] => Course
    [3] => Activity
    [4] => Grade
    [5] => Status
    [6] => Date Graded
    [7] => Expiry Date
    [8] => Status
    [9] => Job Title
    [10] => Cost Centre
    [11] => Store Location
    [12] => Zone
    [13] => State
    [14] => Domain Code
)

	*/

	// Profile Fields

	$classes = '';

	$classes .= ' userid-' . $record->userid;
	$classes .= ' courseid-' . $record->courseid;
	$classes .= ' gradeitemid-' . $record->gradeitemid;
	$classes .= ' coursemoduleid-' . $record->coursemoduleid;

	if (isset($config->reportfields)) {
		
		$fields = explode(',', $config->reportfields);
		
		foreach ($fields as $field) {
		
			if (empty($field)) {
				continue;
			}
			if($field == 'username'){
				$row[] = '<a href="' . $CFG->wwwroot . '/user/profile.php?id=' . $record->userid . '" target="_blank">' . $record->username . '</a>';
			}else if($field == 'fullname'){
				$row[] = '<a href="' . $CFG->wwwroot . '/user/profile.php?id=' . $record->userid . '" target="_blank">' . $record->firstname . ' ' . $record->lastname . '</a>';
			}else if($field == 'coursecategory'){
				$row[] = '<a href="' . $CFG->wwwroot . '/course/management.php?categoryid=' . $record->coursecategoryid . '" target="_blank">' . $record->coursecategory . '</a>';
			}else{
				$row[] = $record->{$field};
			}
			
		}
	}

	$row[] = '<a href="' . $CFG->wwwroot . '/course/view.php?id=' . $record->courseid . '" target="_blank">' . $record->coursefullname . ' (' . $record->courseshortname . ')</a>';

	if ($record->gradeitemtype == 'course') {
		$row[] = '<a href="' . $CFG->wwwroot . '/course/view.php?id=' . $record->courseid . '" target="_blank">' . 'Course Grade' . '</a>';
	} else {

		if (empty($record->gradeitemidnumber)) {
			$row[] = '<a href="' . $CFG->wwwroot . '/course/view.php?id=' . $record->courseid . '" target="_blank">' . $record->gradeitemname . '</a>';
		} else {
			$row[] = '<a href="' . $CFG->wwwroot . '/course/view.php?id=' . $record->courseid . '" target="_blank">' . $record->gradeitemname . ' (' . $record->gradeitemidnumber . ')</a>';
		}


	}

	if ($record->suspended) {
		$classes .= ' suspended';
	}

	$grade = explode(',', $record->{'gradefinalgrade-gradefinaltimemodified'});

	$completion = explode(',', $record->{'completionstate-completiontimemodified'});

	if (report_learnbook_usermodel::useRecurring()) {
		$expiry = explode(',', $record->{'expirystatus-expiryexpirydate'});
	}

	$completionStatus = (int) $completion[0];

	if (report_learnbook_usermodel::useRecurring()) {
		$expiryStatus = (bool) $expiry[0];
	}

	$classes .= ' ' . $record->gradeitemmodule;
	if($config->grade){
		if ($record->gradeitemmodule == 'facetoface') {

			require_once($CFG->dirroot . '/mod/facetoface/lib.php');

			if ($record->{'f2f-status-timestart-timefinish'}) {
			$f2fstatus = @explode(',', $record->{'f2f-status-timestart-timefinish'});
			if (@$f2fstatus[0]) {
				$row[] = ucfirst(facetoface_get_status(@$f2fstatus[0]));
			} else {
				$row[] = '';
			}
			} else {
			$row[] = '';
			}

		} else {

			if (isset($grade[0])) {
				if (is_numeric($grade[0])) {
					$row[] = round($grade[0], 2);

					if (round($grade[0], 2) >= $record->gradeitempass) {
						$completionStatus = true;
					}
					
				} else {
					$row[] = '';
				}
			} else {
				$row[] = '';
			}

		}
    }

	$status = '';

	if (report_learnbook_usermodel::useRecurring()) {
		if ($expiryStatus) {
			$status = 'Expired';
			$classes .= ' expired';
		}
	}

	if ($record->gradeitemtype == 'course') {
		if (isset($completion[1])) {
			$status = 'Completed';
			$classes .= ' completed';
		} else {
			$status = '';
		}
	} else {
		if ($completionStatus == 1) {
			$classes .= ' completed';
			$status = 'Completed';
		} else {
			if ((round($grade[0], 2) >= $record->gradeitempass) && (($record->gradeitempass != 0) && (round($grade[0], 2) != 0))) {
					$status = 'Graded';
			} else {
				$status = '';
			}
		}
	}

	$row[] = report_learnbook_usermodel::reportstatus($record);

	// $row[] = $status;
	if($config->dategrade){
		if (isset($grade[1])) {
			if (is_numeric($grade[1])) {
				$dateGraded = new DateTime();
				$dateGraded->setTimestamp($grade[1]);
				$dateGraded->setTimezone(new DateTimezone('Australia/Sydney'));
				$row[] = $dateGraded->format('d/m/Y');
			}
		} else {
			$row[] = '';
		}
	}
	

	if (report_learnbook_usermodel::useRecurring()) {
		if (isset($expiry[1])) {
			if (is_numeric($expiry[1])) {
				$dateExpired = new DateTime();
				$dateExpired->setTimestamp($expiry[1]);
				$dateExpired->setTimezone(new DateTimezone('Australia/Sydney'));
				$row[] = $dateExpired->format('d/m/Y');
			}
		} else {
			$row[] = '';
		}
	}

	$row['DT_RowClass'] = $classes;

	$data->aaData[] = $row;
	
	

	if(!in_array($record->userid, $temparray, true)){
		array_push($temparray, $record->userid);
		$userdata = new stdClass();
		$userdata->id = $record->userid;
		$userdata->username = $record->username;
		$userdata->firstname = $record->firstname;
		$userdata->lastname = $record->lastname;
		$userdata->email = $record->email;
		$userdata->coursename = $record->coursefullname;
		$userdata->maildisplay = 1;
		$userdata->mailformat = 1;
		array_push($emaildata, $userdata);
    }
	$count++;

}
if (@$CFG->report_learnbook_skipcount == true) {
	// $data->iTotalRecords = 1000000;
	// $data->iTotalDisplayRecords = 1000000;
} else {
	$data->iTotalRecords = $totalCount->count;
	$data->iTotalDisplayRecords = $totalCount->count;
}
if($paramsBulk['SN']){
	$config = get_config('report_learnbook');
	$supportuser = core_user::get_support_user();
	$noreplyaddressdefault = 'noreply@' . get_host_from_url($CFG->wwwroot);
	$noreplyaddress = empty($CFG->noreplyaddress) ? $noreplyaddressdefault : $CFG->noreplyaddress;

	foreach($emaildata as $email){
		$emailbody = str_replace('{{username}}',$email->username, $config->emailbody);
		$emailbody = str_replace('{{fullname}}',$email->coursename, $emailbody);
		$emailbody = str_replace('{{firstname}}',$email->firstname, $emailbody);
		$emailbody = str_replace('{{lastname}}',$email->lastname, $emailbody);
		$emailtxt = html_entity_decode($emailbody);
		email_to_user($email,$noreplyaddress,$config->emailsubject,$emailtxt,$emailbody);
		
	}
echo 'Email Sent Successfully';
}else{
	echo json_encode($data);
}
