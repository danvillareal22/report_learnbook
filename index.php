<?php

require_once('../../config.php');

require_once($CFG->dirroot . '/report/learnbook/usermodel.php');

require_once($CFG->dirroot . '/report/learnbook/lib.php');

require_once($CFG->dirroot.'/user/filters/lib.php');

require_login();

$PAGE->set_pagelayout('frametop');
$PAGE->set_url('/report/learnbook/index.php');
$PAGE->set_context(context_system::instance());

$PAGE->set_title('Training Report');

$PAGE->navbar->add(get_string('report'));
$PAGE->navbar->add('Learnbook Reports');
$PAGE->navbar->add('Training Report', new moodle_url('index.php'));

$PAGE->requires->jquery();

$PAGE->requires->css('/report/learnbook/css/sweetalert2.min.css');

// $PAGE->requires->js(new moodle_url('/report/learnbook/js/jquery.blockUI.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/js/jQuery.extendext.min.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/js/doT.min.js'), true);

$PAGE->requires->css(new moodle_url('/report/learnbook/css/font-awesome.min.css'));
$PAGE->requires->css(new moodle_url('/report/learnbook/css/report.css'));
$PAGE->requires->js(new moodle_url('/report/learnbook/lib/select2/dist/js/select2.js'), true);

$PAGE->requires->css('/report/learnbook/lib/datatables/jquery.dataTables.min.css');
$PAGE->requires->js(new moodle_url('/report/learnbook/lib/datatables/jquery.dataTables.min.js'), true);

$PAGE->requires->js(new moodle_url('/report/learnbook/js/elevator.min.js'), true);

$PAGE->requires->css('/report/learnbook/lib/datatables/dataTables.tableTools.min.css');
$PAGE->requires->js(new moodle_url('/report/learnbook/lib/datatables/dataTables.tableTools.min.js'), true);

$PAGE->requires->css('/report/learnbook/lib/daterangepicker/daterangepicker-bs3.css');
$PAGE->requires->js(new moodle_url('/report/learnbook/lib/daterangepicker/moment.min.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/lib/daterangepicker/daterangepicker.js'), true);

$PAGE->requires->css(new moodle_url('/report/learnbook/lib/select2/dist/css/select2.css'));

//$PAGE->requires->js(new moodle_url('/report/learnbook/js/notification.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/js/report.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/js/jquery.belowfold.min.js'), true);

$PAGE->requires->css('/report/learnbook/css/bootstrap-datepicker.min.css');
$PAGE->requires->css('/report/learnbook/css/query-builder.default.min.css');
$PAGE->requires->js(new moodle_url('/report/learnbook/js/query-builder.min.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/js/bootstrap-datepicker.min.js'), true);

//bootstrap 3
$PAGE->requires->css('/report/learnbook/css/bootstrap.min.css');
$PAGE->requires->js(new moodle_url('/report/learnbook/js/bootstrap.min.js'), true);

$PAGE->requires->css('/report/learnbook/css/bootstrap-slider.min.css');
$PAGE->requires->js(new moodle_url('/report/learnbook/js/bootstrap-slider.min.js'), true);

//sweetalert

$PAGE->requires->js(new moodle_url('/report/learnbook/js/sweetalert2.min.js'), true);

require_once($CFG->dirroot . '/report/learnbook/moodleQueryBuilder.php');

echo moodleQueryBuilder::renderJavascriptVariables();

echo $OUTPUT->header();

?>
<div class="container">
	<div class="row">
		<div class="span7 col-md-7">
			<form class="form-horizontal" id="learnbook_report">
				<legend><i class="fa fa-filter"></i>&nbsp;Filter</legend>
				<div class="form-group">
					<div class="col-sm-2">
						<label class="col-sm-2 control-label" for="cohorts">Cohorts</label>
					</div>
					<div class="col-sm-10">
						<?php echo report_learnbook_report::form_cohort_multiselect(); ?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2">
				    	<label class="col-sm-2 control-label" for="cohorts">Courses</label>
				    </div>
					<div class="col-sm-10">
				    	<?php echo report_learnbook_report::form_course_multiselect(); ?>
				    </div>
				</div>
				<?php

				$config = get_config('report_learnbook');

				if ($config->enableactivitylevelfiltering) { ?>
					<div class="form-group">
						<div class="col-sm-2">
					    	<label class="col-sm-2 control-label" for="cohorts">Activities</label>
							<?php
							$selectedactivities = optional_param_array('report_learnbook_report_activities', array(), PARAM_RAW);
							?>
						</div>
					    <div class="col-sm-10">
						    <select multiple id="report_learnbook_report_activities" name="report_learnbook_report_activities[]">
						    <?php
							    if (!empty($selectedactivities)) {
							    	$data = report_learnbook_report::constructGradeItemSelector();
							    	foreach ($selectedactivities as $itemid) {
							    		echo '<option selected value="' . $itemid . '">' . $data[$itemid] . '</option>';
							    	}
							    }
						    ?>	
						    </select>
					    </div>
					</div>
				<?php }
				$searchterm = optional_param('searchterm', '', PARAM_RAW);
				?>
				<input type="hidden" id="report_learnbook_search_type" value="<?php echo $searchterm; ?>" name="searchterm" />
				<div class="form-group">
					<?php
						$daterange = optional_param('daterange', '', PARAM_RAW);
					?>
					<div class="col-sm-2">
						<label class="col-sm-2 control-label" for="learnbook_report_daterange">Date Range</label>
					</div>
					<div class="col-sm-4">
				    	<input id="learnbook_report_daterange" placeholder="Select a Date Range" value="<?php echo $daterange; ?>" type="text" name="daterange" />
				    </div>
					<div class="col-sm-1">
				    	<label class="col-sm-1 control-label" for="cohorts">Status</label>
					</div>
				    <div class="col-sm-2">
						<?php
							$statuses = array();
							$statuses[-1] = 'Any';
							$statuses['complete'] = 'Completed';
							$statuses['notcomplete'] = 'Not Completed';
							if (report_learnbook_usermodel::useRecurring()) {
								$statuses['expired'] = 'Expired';
								$statuses['notexpired'] = 'Not Expired';
							}
							$status = optional_param('status', array(-1), PARAM_RAW);
							echo html_writer::select($statuses, 'status', $status, false, array('id' => 'learnbook_report_status', 'required' => 'false'));
						?>
					</div>
					<div class="col-sm-3">
						<span class="pull-right" style="text-align: right; line-height: 1;">
							<label for="cohorts">Skip Inactive</label>
					    	<?php echo report_learnbook_report::form_course_suspendedusers(); ?>
					    	<label for="cohorts">Course Grades</label>
					    	<?php
						    $includeCourseGrades = optional_param('includeCourseGrades', 0, PARAM_INT);
							echo '<input type="hidden" value="0" name="includeCourseGrades" />' . html_writer::checkbox('includeCourseGrades',"1", $includeCourseGrades,'', array('id' => 'learnbook_report_includeCourseGrades'));
						    ?>
					    </span>
				    </div>
				</div>
				<div class="form-group">
					<input type="hidden" name="queryBuilderState" value='<?php echo optional_param('queryBuilderState', '{"condition":"AND","rules":[]}', PARAM_RAW); ?>' /> 
					<input type="hidden" name="queryBuilderQuery" />
				</div>
			</form>
			<div class="row">
				<div class="col-sm-10">
					<?php
					$config = get_config('report_learnbook');
					if ($config->enableadvancedfilters) {
						echo '<div id="builder"></div>';
					} else {
						echo '<div style="display: none !important;" id="builder"></div>';
					}
					?>
				</div>
				<div class="col-sm-2">
					<button id="runreport" class="btn btn-primary pull-right"><i class="fa fa-arrow-right"></i>&nbsp;Run Report</button>
				</div>
			</div>
		</div>
		<div class="span5 col-md-5">
			<?php

			$config = get_config('report_learnbook');

			if ($config->enablescheduling) {

			?>
			<legend><i class="fa fa-bookmark"></i>&nbsp;Saved Reports</legend>
<?php
	$reports = $DB->get_records_sql("SELECT * FROM {report_learnbook} WHERE userid = ? ORDER BY id DESC", array($USER->id));
?>
				<table class="table table-striped table-bordered">
					<tbody style="display: block; height: 200px; overflow-y: auto; overflow-x: hidden;">
<?php

		if (!count($reports)) {
			?>

			<tr style="height:200px;">
				<td style="width: 100%;"><i>There are no scheduled or saved reports available.</i></td>
				<td colspan="2">&nbsp;</td>
			</tr>

			<?php
		}

		foreach ($reports as $report) {

			$time = '<i>Processing</i>';
			if ($report->processed) {
				$time = new DateTime();
				$time->setTimestamp($report->processed);
				$time->setTimeZone(new DateTimeZone('Australia/Sydney'));
				$time = $time->format('H:i d/m');
			}

			$name = 'Report ID: ' . $report->id;

			if ($report->name) {
				$name = $report->name;
			}

			if ($report->schedule != 'now') {
				$name .= ' <i>(scheduled ' . $report->schedule . ')</i> ';
			} else {
				$name .= ' <i>(Ad-Hoc)</i>';
			}

			ob_start();
			echo '<a href="' . $CFG->wwwroot . '/report/learnbook/index.php?' . $report->params . '">';
			// if ($config->usefontawesome) {
			// 	echo '<i class="fa fa-external-link-square"></i>';
			// }
			echo $name;
			echo '</a>';
			$link = ob_get_clean();

?>	
					<tr>
						<td style="width: 100%;"><?php echo $link; ?></td>
						<td style="white-space: nowrap;"><?php echo $time; ?></td>
						<td style="white-space: nowrap;">
							<?php
							if ($report->processed) {
									
								$config = get_config('report_learnbook');

								if ($config->usefontawesome) {
									echo '<a href="' . $CFG->wwwroot . '/report/learnbook/downloadsavedreport.php?id=' . $report->id . '"><i class="fa fa-download"></i></a>';
								} else {
									echo '<a href="' . $CFG->wwwroot . '/report/learnbook/downloadsavedreport.php?id=' . $report->id . '"><i class="fa fa-download">Download</i></a>';
								}
								
							} else {
								echo '<i class="fa fa-cog fa-spin"></i>';
							}

							echo '&nbsp;';

							// if ($report->schedule != 'now') {
								if ($config->usefontawesome) {
									echo '<a href="' . $CFG->wwwroot . '/report/learnbook/deletesavedreport.php?id=' . $report->id . '"><i class="fa fa-trash"></i></a>';
								} else {
									echo '<a href="' . $CFG->wwwroot . '/report/learnbook/deletesavedreport.php?id=' . $report->id . '"><i class="fa fa-trash">Delete</i></a>';
								}
							// }

							?>
						</td>
					</tr>
<?php
		}
?>
					</tbody>
				</table>
				<form id="learnbook_report_savedreports" class="pull-right">
					<?php

					$config = get_config('report_learnbook');

					if ($config->enablecustomscheduling) {

					?>
				
						<input required type="text" id="learnbook_report_savedreports_reportname" name="reportname" placeholder="Report Name" />
					
						<input  required type="text" id="learnbook_report_savedreports_email" name="emailaddress" placeholder="Email" />

						<select id="learnbook_report_savedreports_reportschedule" name="reportschedule">
							<option value="now">Now</option>
							<option value="today">Everyday</option>
							<option value="monday">Every Monday</option>
							<option value="tuesday">Every Tuesday</option>
							<option value="wednesday">Every Wednesday</option>
							<option value="thursday">Every Thursday</option>
							<option value="friday">Every Friday</option>
							<option value="saturday">Every Saturday</option>
							<option value="sunday">Every Sunday</option>
							<option value="first day of this month">First Day of Month</option>
							<option value="last day of this month">Last Day of Month</option>
						</select>
						<?php
						} else {
							echo '<input type="hidden" id="learnbook_report_savedreports_reportname" value="" />';
							echo '<input type="hidden" id="learnbook_report_savedreports_reportschedule" value="now" />';
						}
						?>

					
						<button class="btn btn-info"><i class="fa fa-envelope"></i></button>
					<br /><br />
				</form>
		</div>
		<?php } // Config Scheduled Enabled ?>
	</div>
	<div class="row">
		<div class="span12">
			<div id="learnbook_report_datatables"></div>
			<?php echo serverreport::renderDataTable(); ?>
			<a href="#" class="elevator btn pull-right">Back to Top</a>
		</div>
		<a href="<?php echo $CFG->wwwroot . '/admin/user/user_bulk.php'; ?>" class="btn btn-primary" target="_blank">Bulk User Actions</a>
	</div>
</div>

<?php

echo $OUTPUT->footer();

?>
