<?php
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 20151116013;    // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2015111000;    // Requires this Moodle version
$plugin->component = 'report_learnbook';  // Full name of the plugin (used for diagnostics)
