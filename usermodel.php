<?php
  
  // ini_set('memory_limit','1024M');

  defined('MOODLE_INTERNAL') || die;

  class report_learnbook_usermodel {

    public static function reportstatus($record) {

      if (is_array($record)) {
        $record = json_decode(json_encode($record), FALSE);
      }

      $status = '';

      $completion = explode(',', $record->{'completionstate-completiontimemodified'});

      if ($record->gradeitemtype == 'course') {

        if (isset($completion[1])) {
          $status = 'Completed';
        } else {
          $status = '';
        }

      } else {

        $completionStatus = (int) $completion[0];

        if ($completionStatus == 1) {
          
          $status = 'Completed';

        } else {

          $grade = explode(',', $record->{'gradefinalgrade-gradefinaltimemodified'});

          if ((round($grade[0], 2) >= $record->gradeitempass) && (!empty($grade[0]) && ($grade[0] != 0) )) {
            $status = 'Passed';
          }

        }

      }

      if (report_learnbook_usermodel::useRecurring()) {
        $expiry = explode(',', $record->{'expirystatus-expiryexpirydate'});
      }

      if (report_learnbook_usermodel::useRecurring()) {
        $expiryStatus = (bool) $expiry[0];
        if ($expiryStatus) {
          $status = 'Expired';
        }
      }

      return $status;

    }

    public static function fetchUserCohorts($userid) {

      global $DB;
      $cohortmanagershortname = get_string('cohortmanagershortname','report_learnbook');
      $managerCohorts = array();
      $csvcohorts = $DB->get_records_sql("SELECT data
                                      FROM {user_info_field} UIF, {user_info_data} UID
                                      WHERE UIF.datatype = 'cohortselect'
                                      AND UIF.shortname = ?
                                      AND UIF.id = UID.fieldid
                                      AND UID.userid = ?
                                      ", array($cohortmanagershortname, $userid));
      foreach ($csvcohorts as $csvcohort) {
        $cohortids = explode(',', $csvcohort->data);
        foreach ($cohortids as $cohort) {
          if (!is_numeric($cohort)) { // Ensure no weird cohorts are saved...
            continue;
          } else {
            $c = $DB->get_record_sql("SELECT * FROM {cohort} C WHERE C.id = ?", array($cohort));
            if ($c) {
              $managerCohorts[$cohort] = $c;
            }

          }
        }
      }
      return $managerCohorts;
    }
    public static function fetchUserCohortMembership($userid) {
      global $DB;
      $query = $DB->get_records_sql("SELECT C.id FROM {cohort} C, {cohort_members} CM WHERE CM.cohortid = C.id AND CM.userid = ?", array($userid));
      $cohorts = array();

      foreach ($query as $cohort) {
        array_push($cohorts, $cohort->id);
      }

      return $cohorts;
    }


    public static function fetchUserManagers($userid) {

      global $DB;

      $csvcohortswithmanagers = $DB->get_records_sql("SELECT UID.*
                                      FROM {user_info_field} UIF, {user_info_data} UID
                                      WHERE UIF.datatype = 'cohortselect'
                                      AND UIF.id = UID.fieldid
                                      ");

      $cohorts = array();
      $userCohorts = $DB->get_records_sql("SELECT * FROM {cohort_members} CM WHERE CM.userid = ?", array($userid));
      $userManagers = array();
      foreach ($userCohorts as $userCohort) {
          
        foreach ($csvcohortswithmanagers as $csvcohortwithmanager) {
          $cohortids = explode(',', $csvcohortwithmanager->data);
          if (in_array($userCohort->cohortid, $cohortids)) {
           $userManagers[$userCohort->cohortid] = $userCohort->cohortid;
           $userManagers[$csvcohortwithmanager->userid] = $csvcohortwithmanager->userid; 
          
          }
        }
      }
      //$userManagers = array_keys($userManagers);
      return $userManagers;
    }

    public static function fetchManagerChildren($userid, $flat = true) {
      
      global $DB;

      $managerCohorts = array();

      $csvcohorts = $DB->get_records_sql("SELECT data
                                      FROM {user_info_field} UIF, {user_info_data} UID
                                      WHERE UIF.datatype = 'cohortselect'
                                      AND UIF.id = UID.fieldid
                                      AND UID.userid = ?
                                      ", array($userid));
      
      foreach ($csvcohorts as $csvcohort) {
        $cohortids = explode(',', $csvcohort->data);
        foreach ($cohortids as $cohort) {
          if (!is_numeric($cohort)) { // Ensure no weird cohorts are saved...
            continue;
          } else {
            $members = $DB->get_records_sql("SELECT userid as 'id' FROM {cohort_members}  WHERE cohortid = ?", array($cohort));
            if (count($members)) {
              $members = array_keys($members);
              $managerCohorts[$cohort] = $members;
            }
          }
        }
      }


      // Return a flat list of children of a tree based on their cohort?

      if ($flat) {
        return self::fetchManagerChildren_flat($managerCohorts);
      } else {
        return $managerCohorts;
      }

    }

    public static function fetchUserPicture($userid, $options = array("size"=>100, 'link'=>true) ) {
      global $OUTPUT,$DB;
      $userObj = $DB->get_record_select("user", "id = ?", array($userid) );

      return $OUTPUT->user_picture($userObj, $options);
    }

    public static function fetchManagerChildren_flat($managerCohorts) {
      $children = array();
      foreach ($managerCohorts as $cohort) {
        foreach ($cohort as $cohortmember) {
          $children[$cohortmember] = null;
        }
      }
      return array_keys($children);
    }

    public static function get_fast_modinfo($courseid, $userid) {
      $modinfocache = cache::make_from_params(cache_store::MODE_REQUEST, 'report_learnbook', 'modinfo');
      $modinfocachekey = $courseid . '-' . $userid;
      $modinfo = $modinfocache->get($modinfocachekey);
      if ($modinfo) {
        return $modinfo;
      } else {
        $modinfo = get_fast_modinfo($courseid, $userid);
        // $modinfocache->set($modinfocachekey, $modinfo);
        return $modinfo;
      }
    }

    public static function fetchCoursePicture($courseid) {
        global $DB, $CFG;

        $course = $DB->get_record_select("course", "id = ?", array($courseid) );

        if ($course instanceof stdClass) {
            require_once($CFG->libdir. '/coursecatlib.php');
            $course = new course_in_list($course);
        }

        $contentimages = $contentfiles = '';
        if ($course->get_course_overviewfiles()) {
          foreach ($course->get_course_overviewfiles() as $file) {
                
              $isimage = $file->is_valid_image();
              $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                      '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                      $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
              if ($isimage) {
                  $contentimages .= html_writer::empty_tag('img', array('src' => $url, 'class' => 'coursepicture'));
              } else {
                  $image = $this->output->pix_icon(file_file_icon($file, 24), $file->get_filename(), 'moodle');
                  $filename = html_writer::tag('span', $image, array('class' => 'fp-icon')).
                          html_writer::tag('span', $file->get_filename(), array('class' => 'fp-filename'));
                  $contentfiles .= html_writer::tag('span',
                          html_writer::link($url, $filename),
                          array('class' => 'coursefile fp-filename-icon'));
              }
          }
        }else{
          $url = "/theme/learnbook/pix/courseimage_laptop.jpg";
          $contentimages .= html_writer::start_tag('div', array('class' => 'span2 coursewithoutpicture','style' => 'border-left: 10px solid '.@$colorval.' '));
          $contentimages .= html_writer::empty_tag('img', array('src' => $url, 'class' => 'coursepicture'));     
          $contentimages .= html_writer::end_tag('div'); // .coursenamewrap
        }

        return $contentimages;

    }

    public static function courseCertificateStatus($userid, $courseid) {

      ob_start();
      $coursemodinfo = self::get_fast_modinfo($courseid, $userid);
      ob_get_clean();
      $cm = @$coursemodinfo->instances['certificate'];
      if ($cm) {
        $cminstance = reset($cm);
        $certificatestatus = new stdClass();
        $certificatestatus->hasCertificate = true;
        $certificatestatus->courseModuleID = (int) $cminstance->id;
        $certificatestatus->certificateName = (string) $cminstance->name;
        $certificatestatus->isAccessible = (bool) $cminstance->uservisible;
        return $certificatestatus;
      } else {
        $certificatestatus = new stdClass();
        $certificatestatus->hasCertificate = false;
        $certificatestatus->courseModuleID = null;
        $certificatestatus->certificateName = null;
        $certificatestatus->isAccessible = false;
        return $certificatestatus;
      }
      
    }

    public static function isCourseModuleAccessible($userid, $courseid, $modname, $instanceid) {
      ob_start();
      $coursemodinfo = self::get_fast_modinfo($courseid, $userid);
      $cm = $coursemodinfo->instances[$modname][$instanceid];
      ob_get_clean();
      return @$cm->uservisible;
    }
    
    public static function fetchUserDetails($userid) {
      global $CFG, $DB;
      $usercache = cache::make_from_params(cache_store::MODE_REQUEST, 'report_learnbook', 'users');
      $user = $usercache->get($userid);
      if ($user) {
        return $user;
      } else {
        require_once($CFG->dirroot . '/user/profile/lib.php');
        $user = $DB->get_record_sql("SELECT id, auth, confirmed, policyagreed, deleted, suspended, mnethostid, username, idnumber, firstname, lastname, email, icq, skype, yahoo, aim, msn, phone1, phone2, institution, department, address, city, country, lang, calendartype, theme, timezone, firstaccess, lastaccess, lastlogin, currentlogin, lastip, secret, picture, url, description, descriptionformat, mailformat, maildigest, maildisplay, autosubscribe, trackforums, timecreated, timemodified, trustbitmask, imagealt, lastnamephonetic, firstnamephonetic, middlename, alternatename FROM {user} U WHERE U.id = ?", array($userid));
        // $usercohorts = $DB->get_records_sql("SELECT C.id, C.name FROM {cohort} C, {cohort_members} CM WHERE CM.cohortid = C.id AND CM.userid = ?", array($userid));
        profile_load_data($user);
        // $user->cohorts = $usercohorts;
        // $usercache->set($userid, $user);
        return $user;
      }
    }

    public static function preloadUserIDs($userids = array()) {

      // Optimized Query to fetch lots of users and make a warm cache before querying them...

      global $DB;
      $uifs = $DB->get_records("user_info_field");
      $userprofileselects = array();
      foreach ($uifs as $uif) {
        $userprofileselect[] = "(SELECT UID.data FROM {user_info_data} UID WHERE UID.fieldid = " . $uif->id . " AND UID.userid = U.id) as 'profile_field_" . $uif->shortname . "'";
      }
      $userprofileselect = implode(",\n", $userprofileselect);
      $query = "
        SELECT U.*,
        $userprofileselect
        FROM {user} U
      ";
      if (count($userids)) {
        $query .= " WHERE U.id IN (" . implode(',', $userids) . ")";
      }
      $x = $DB->get_recordset_sql($query);
      $usercache = cache::make_from_params(cache_store::MODE_REQUEST, 'report_learnbook', 'users');
      foreach ($x as $user) {
        // $usercache->set($user->id, $user);
      }
    }

    public static function useRecurring() {

      global $CFG, $DB;

      $config = get_config('report_learnbook');

      if ($config->disablerecurring) {
        return false;
      }

      $cache = cache::make_from_params(cache_store::MODE_REQUEST, 'report_learnbook', 'useRecurringCache');

      $useRecurring = $cache->get('useRecurring');

      if ($useRecurring === 'true') {
        return true;
      } else if ($useRecurring === 'false') {
        return false;
      }

      $useRecurring = $DB->get_record_sql("SHOW TABLES LIKE '" . $CFG->prefix . "block_recurring_traininginfo'");
      $useRecurringKeys = (array) $useRecurring;
      $useRecurringKeys = array_keys($useRecurringKeys);

      $result = @$useRecurring->{@$useRecurringKeys[0]};

      if (empty($result)) {
        $cache->set('useRecurring', 'false');
        $result = false;
      } else {
        $cache->set('useRecurring', 'true');
        $result = true;
      }

      return $result;

    }

    public static function fetchUsers($userids = array(), $courseids = array(), $options = array(), $params = false) {
      // ob_start();
      global $CFG, $DB;

      $startdate = @$options['startdate'];
      $enddate = @$options['enddate'];
      $limit = @$options['limit'];
      $offset = @$options['offset'];
      $flat = @$options['flat'];
      $completed = @$options['completed'];
      $status = @$options['status'];
      $includeCourseGrades = @$options['includeCourseGrades'];
      $returnRecordSet = @$options['returnRecordSet'];
      $returnTotalCount = @$options['returnTotalCount'];
      $excludeSuspended = @$options['excludesuspended'];
      $userRecords = array();
      $useRecurring = self::useRecurring();
      if ($startdate && $enddate) {
        $strWhen = "AND ((SELECT timemodified FROM {grade_grades} GG WHERE GG.itemid = GI.id AND GG.userid = U.id) >= $startdate)
                    AND ((SELECT timemodified FROM {grade_grades} GG WHERE GG.itemid = GI.id AND GG.userid = U.id) <= $enddate)";
      } else {
        $strWhen = '';
      }

      $searchterm = '';

      $updateBulkActionsSelection = false;

      if ($params) {

        $updateBulkActionsSelection = @$params['updateBulkActionsSelection'];

        if (!empty($params['searchterm'])) {
          $searchterm = $params['searchterm']; // CLI
        }

        if (!empty($params['search']['value'])) {
          $searchterm = $params['search']['value']; // Web
        }

        $includeCourseGrades = @$params['includeCourseGrades'];

        if (!empty($params['report_learnbook_report_activities'])) {

          $activities = array();

          foreach ($params['report_learnbook_report_activities'] as $activity) {
            if (is_numeric($activity)) {
              $activities[] = $activity;
            }
          }

          if (!empty($activities)) {
            $activities = implode(',', $activities);
            $string = ' AND GI.id IN (' . $activities . ') ';
            $strWhen .= $string;
          }
        }

      }

      if (!$includeCourseGrades) {
        $strWhen .= " AND GI.itemtype != 'course'";
      }

      if ($excludeSuspended) {
        $strWhen .= " AND U.suspended = 0 ";
      } else {
      
      }

      if ($status == 'expired') {
        $strWhen .= " AND U.id IN (SELECT TI.userid FROM {block_recurring_traininginfo} TI WHERE TI.courseid = GI.courseid AND TI.status = 1) ";
      } else if ($status == 'notexpired') {
        $strWhen .= " AND U.id NOT IN (SELECT TI.userid FROM {block_recurring_traininginfo} TI WHERE TI.courseid = GI.courseid AND TI.status = 1) ";
      } else if ($status == 'complete') {
        $strWhen .= " AND (
                            ((U.id IN (SELECT GG.userid FROM {grade_grades} GG WHERE GG.userid = U.id AND GG.itemid = GI.id AND GG.finalgrade >= GI.gradepass AND GI.itemtype != 'course' AND GG.finalgrade != 0))
                            OR
                            (U.id IN (SELECT CMC.userid FROM {course_modules_completion} CMC WHERE CMC.coursemoduleid = CM.id AND CMC.completionstate = 1 AND GI.itemtype != 'course')))
                            OR
                            (U.id IN (SELECT CC.userid FROM {course_completions} CC WHERE CC.course = GI.courseid AND CC.timecompleted IS NOT NULL AND GI.itemtype = 'course'))
                          ) ";
      } else if ($status == 'notcomplete') {

        $strWhen .= " AND (
                            ((GI.itemtype != 'course' AND (U.id NOT IN (SELECT GG.userid FROM {grade_grades} GG WHERE GG.userid = U.id AND GG.itemid = GI.id AND GG.finalgrade >= GI.gradepass AND GG.finalgrade!= 0)))
                            AND NOT
                            (GI.itemtype != 'course' AND NOT (U.id NOT IN (SELECT CMC.userid FROM {course_modules_completion} CMC WHERE CMC.coursemoduleid = CM.id AND CMC.completionstate = 1))))
                            OR
                            (GI.itemtype = 'course' AND (U.id NOT IN (SELECT CC.userid FROM {course_completions} CC WHERE CC.course = GI.courseid AND CC.timecompleted IS NOT NULL)))
                          ) ";

      }

      /*

  (CASE GI.itemtype
            WHEN 'course' THEN (SELECT CONCAT('', ',', timecompleted) FROM mdl_course_completions CM WHERE CM.userid = U.id AND CM.course = C.id)
                      WHEN 'mod' THEN (SELECT CONCAT(completionstate, ',', timemodified) FROM mdl_course_modules_completion CMM WHERE CMM.userid = U.id AND CMM.coursemoduleid = CM.id)
                  END) as 'completionstate-completiontimemodified',

      */

      $userids = implode(',', $userids);
      $courseids = implode(',', $courseids);

      $strWhere = '';
      if ($userids) {
        $strWhere .= ' AND U.id IN (' . $userids . ') ';
      } else {
        // return array();
      }
      if ($courseids) {
        $strWhere .= ' AND C.id IN (' . $courseids . ') ';
      } else {
        // return array();
      }

      $learnbookconfig = get_config('report_learnbook');
      
      $userstatements = array();

      $usersearchtermstatements = array();

      if (@$learnbookconfig->reportfields) {
        $fields = explode(',', $learnbookconfig->reportfields);
        foreach ($fields as $field) {
          if (substr($field, 0, 14) == 'profile_field_') { // User Profile Field
            $shortname = substr($field, 14);
            $userstatements[] = "(SELECT UID.data FROM {user_info_field} UIF, {user_info_data} UID WHERE UIF.id = UID.fieldid AND UID.userid = U.id AND UIF.shortname = '$shortname') as '$field',";
            $usersearchtermstatements[] = "((SELECT UID.data FROM mdl_user_info_field UIF, mdl_user_info_data UID WHERE UIF.id = UID.fieldid AND UID.userid = U.id AND UIF.shortname = '$shortname' AND UID.data LIKE '%$searchterm%') IS NOT NULL)";
          } else if ($field == 'earliestenrolment') {
            $userstatements[] = "(SELECT FROM_UNIXTIME(MIN(UE.timecreated), '%d/%m/%Y')
            FROM {user_enrolments} UE, {enrol} E
            WHERE UE.enrolid = E.id AND UE.userid = U.id
            AND E.courseid = C.id
            GROUP BY UE.userid) as 'earliestenrolment',";
          } else if ($field == 'scorminteractiontime') {
            $userstatements[] = " (SELECT SST.value
                                  FROM {scorm_scoes_track} SST
                                  WHERE GI.itemmodule = 'scorm'
                                  AND SST.scormid = GI.iteminstance
                                  AND SST.element = 'cmi.core.total_time'
                                  AND SST.userid = U.id
                                  AND SST.attempt = (SELECT MAX(SST2.attempt) FROM {scorm_scoes_track} SST2 WHERE SST2.scormid = SST.scormid AND SST2.userid = SST.userid)) as 'scorminteractiontime',";
          } else if ($field == 'cohorts') {

            if ($flat) {
              $userstatements[] = " (SELECT GROUP_CONCAT(C.name SEPARATOR ', ')
                                  FROM {cohort} C, {cohort_members} CM
                                  WHERE C.id = CM.cohortid AND CM.userid = U.id
                                  GROUP BY CM.userid) as 'cohorts',";
            } else {
              $userstatements[] = " (SELECT GROUP_CONCAT(C.name SEPARATOR '<br />')
                                  FROM {cohort} C, {cohort_members} CM
                                  WHERE C.id = CM.cohortid AND CM.userid = U.id
                                  GROUP BY CM.userid) as 'cohorts',";
            }
            
          } else { // User Field
            if ($field == 'timecreated') {
              $userstatements[] = "(FROM_UNIXTIME(U.timecreated, '%d/%m/%Y')) as 'timecreated',";
            } else if ($field == 'cmhours') {
              $userstatements[] = "(CM.cmhours) as 'cmhours',";
            } else if ($field == 'fullname'){
              $userstatements[] = "CONCAT(U.firstname, ' ', U.lastname) as '$field',";
            } else if ($field == 'coursecategory'){
              $userstatements[] = "(COC.name) as '$field',";
            } else {
              $userstatements[] = "(SELECT U.$field) as '$field',";
              $usersearchtermstatements[] = "(U.$field LIKE '%$searchterm%')";
            }
            
          }
        }
      }

      $queryuserstatements = '';
      foreach ($userstatements as $userstatement) {
        $queryuserstatements .= "$userstatement\n";
      }

      if (!empty($searchterm)) {

        $usersearchtermstatements[] = "(U.username LIKE '%$searchterm%')";
        $usersearchtermstatements[] = "(U.firstname LIKE '%$searchterm%')";
        $usersearchtermstatements[] = "(U.lastname LIKE '%$searchterm%')";
        $usersearchtermstatements[] = "(C.fullname LIKE '%$searchterm%')";
        $usersearchtermstatements[] = "(C.shortname LIKE '%$searchterm%')";
        $usersearchtermstatements[] = "(C.idnumber LIKE '%$searchterm%')";
        $usersearchtermstatements[] = "(GI.itemname LIKE '%$searchterm%')";
        $usersearchtermstatements[] = "(GI.idnumber LIKE '%$searchterm%')";

        $searchfilter = " AND (\n";
        foreach ($usersearchtermstatements as $key => $usts) {

          if ($key == 0) {
            $searchfilter .= " $usts \n";
          } else {
            $searchfilter .= " OR $usts \n";
          }
          
        }
        $searchfilter .= ")\n";
        
        $strWhen .= $searchfilter;

      }

      if ($updateBulkActionsSelection) {

        global $SESSION;

        $orderby = '';

        if (@$CFG->lb_usermodel_ascendingcourses) {
          $orderby = 'ORDER BY C.startdate DESC';
        }

        $query = "
            SELECT
              DISTINCT U.id as 'id'
        FROM {user} U, {course} C, {grade_items} GI
        LEFT JOIN {modules} M
        ON M.name = GI.itemmodule
        LEFT JOIN {course_modules} CM
        ON GI.iteminstance = CM.instance AND CM.course = GI.courseid AND CM.module = M.id
        WHERE
        GI.courseid = C.id
        AND U.deleted = 0
        $strWhere
        $strWhen
        AND U.id IN (SELECT UE.userid FROM {user_enrolments} UE, {enrol} E WHERE E.id = UE.enrolid AND E.status = 0 AND E.courseid = C.id AND UE.userid = U.id AND E.status = 0)
        $orderby
        ";

        $userids = $DB->get_records_sql($query);

        $users = array();

        foreach ($userids as $ukey => $u) {
          $users[$ukey] = $ukey;
        }

        $SESSION->bulk_users = $users;

      }

      $queryBuilderQuery = json_decode($params['queryBuilderState']);

      $queryParams = array();

      if (!empty($queryBuilderQuery->rules)) {

        require_once($CFG->dirroot . '/report/learnbook/moodleQueryBuilder.php');

        $mqb = new moodleQueryBuilder();
        $parsedSQL = $mqb->parseState($queryBuilderQuery);
        $queryParams = $mqb->params;
        // $parsedSQL = moodleQueryBuilder::parseState($queryBuilderQuery);

        if (!empty($parsedSQL)) {
          $strWhen .= 'AND (' . $parsedSQL . ')';
          $queryParams = $mqb->params;
        }

        // foreach ($input->rules as $key => $rule) {

        //   if ($rule->id == '%%php:isaccessible%%') {
        //     print_r($rule);
        //     die('php is accessible is set');
        //   } else {
        //     continue;
        //   }

        // }
       
        // PHP Manipulators...

      }

      

      // $strWhen .= 'AND (' . $sql . ')'

      if ($returnTotalCount) {

        $query = "
            SELECT
              COUNT(CONCAT('-', U.id, GI.id, C.id)) as 'count' 
        FROM {user} U, {course} C, {course_categories} COC, {grade_items} GI
        LEFT JOIN {modules} M
        ON M.name = GI.itemmodule
        LEFT JOIN {course_modules} CM
        ON GI.iteminstance = CM.instance AND CM.course = GI.courseid AND CM.module = M.id
        WHERE
        GI.courseid = C.id
        AND U.deleted = 0
        AND COC.id = C.category
        $strWhere
        $strWhen
        AND U.id IN (SELECT UE.userid FROM {user_enrolments} UE, {enrol} E WHERE E.id = UE.enrolid AND E.status = 0 AND E.courseid = C.id AND UE.userid = U.id AND E.status = 0)
        ";

      } else {

        $recurring = '';

        if ($useRecurring) {
          $recurring = ", (SELECT CONCAT(status, ',', expirydate) FROM {block_recurring_traininginfo} TI WHERE TI.courseid = GI.courseid AND TI.userid = U.id LIMIT 1) as 'expirystatus-expiryexpirydate'";
        }

        $orderby = '';

        if (@$CFG->lb_usermodel_ascendingcourses) {
          $orderby = 'ORDER BY C.startdate DESC';
        }

        $query = "
            SELECT
              CONCAT_WS('-', U.id, GI.id, C.id) as 'id',
            U.id as 'userid',
            U.username as 'username',
            U.firstname as 'firstname',
            U.lastname as 'lastname',
            U.email as 'email',
            U.suspended as 'suspended',
            $queryuserstatements
            C.id as 'courseid',
            C.fullname as 'coursefullname',
            C.shortname as 'courseshortname',
            C.visible as 'coursevisible',
            COC.id as 'coursecategoryid',
            COC.name as 'coursecategory',
            GI.itemname as 'gradeitemname',
            GI.idnumber as 'gradeitemidnumber',
            GI.itemtype as 'gradeitemtype',
            GI.gradepass as 'gradeitempass',
            GI.itemmodule as 'gradeitemmodule',
            GI.iteminstance as 'gradeiteminstance',
            CM.id as 'coursemoduleid',
            GI.id as 'gradeitemid',
                  (CASE GI.itemtype
            WHEN 'course' THEN (SELECT CONCAT('', ',', timecompleted) FROM {course_completions} CM WHERE CM.userid = U.id AND CM.course = C.id)
                      WHEN 'mod' THEN (SELECT CONCAT(completionstate, ',', timemodified) FROM {course_modules_completion} CMM WHERE CMM.userid = U.id AND CMM.coursemoduleid = CM.id)
                  END) as 'completionstate-completiontimemodified',
            
            (SELECT CONCAT(finalgrade, ',', timemodified) FROM {grade_grades} GG WHERE GG.itemid = GI.id AND GG.userid = U.id) as 'gradefinalgrade-gradefinaltimemodified'
            $recurring  
        FROM {user} U, {course} C, {course_categories} COC, {grade_items} GI
        LEFT JOIN {modules} M
        ON M.name = GI.itemmodule
        LEFT JOIN {course_modules} CM
        ON GI.iteminstance = CM.instance AND CM.course = GI.courseid AND CM.module = M.id
        WHERE
        GI.courseid = C.id
        AND COC.id = C.category
        AND U.deleted = 0
        AND U.id IN (SELECT UE.userid FROM {user_enrolments} UE, {enrol} E WHERE E.id = UE.enrolid AND E.status = 0 AND E.courseid = C.id AND UE.userid = U.id AND E.status = 0)
        $strWhere
        $strWhen
        $orderby
        ";

      }

      if ($returnTotalCount) {
        return $DB->get_records_sql($query, $queryParams);
      }

      if (is_numeric($limit)) {
        $query .= ' LIMIT ' . $limit;

        if (is_numeric($offset)) {
          $query .= ' OFFSET ' . $offset;
        }

      }



      $x = $DB->get_recordset_sql($query, $queryParams);
      if (!$flat) {
       if (!$x->valid()) {
          return array();
        }
      }

      if ($returnRecordSet) {
        return $x;
      }

      if ($flat) {

        $madeHeader = false;

        $filename = uniqid('report_learnbook_report_', true) . '.csv';

        $fileid = $CFG->dataroot . '/' . $filename;

        $fp = fopen($fileid, 'w');

        foreach ($x as $row) {

          $row = (array) $row;

          /*
            ID
            Name
            Course
            Activity
            Grade
            Date Graded
            Status
            Fields...
          */

          // Logic...

          if ($row['gradeitemtype'] == 'course') {
            $row['activity'] = 'Course Grade';
          } else {
            $row['activity'] = $row['gradeitemname'];
          }

          $grade = @explode(',', $row['gradefinalgrade-gradefinaltimemodified']);
          $timegraded = '';
          if (@is_numeric($grade[1])) {
            $timegraded = new DateTime();
            $timegraded->setTimestamp($grade[1]);
            $timegraded->modify('midnight');
            $timegraded->setTimeZone(new DateTimeZone('Australia/Sydney'));
            $timegraded = $timegraded->format('d/m/Y');
          } else {
            $timegraded = '';
          }

          if (self::useRecurring()) {
            $expiry = @explode(',', $row['expirystatus-expiryexpirydate']);
            $timeexpired = '';
            if (@is_numeric($expiry[1])) {
              $timeexpired = new DateTime();
              $timeexpired->setTimestamp($expiry[1]);
              $timeexpired->setTimeZone(new DateTimeZone('Australia/Sydney'));
              $timeexpired = $timeexpired->format('d/m/Y');
            } else {
              $timeexpired = '';
            }
            $expired = (bool)$expiry[0];
          } else {
            $expired = false;
          }

          $status = '';

          $completion = @explode(',', $row['completionstate-completiontimemodified']);

          if ($expired) {
            $status = 'Expired';
          } else {
            if ($completion[0]) {
              $status = 'Complete';
            }
          }

          if ($grade[0]) {
            $row['grade'] = round($grade[0], 2);
          } else {
            $row['grade'] = '';
          }

          $row['date'] = @$timegraded;
          if (self::useRecurring()) {
            $row['expirydate'] = @$timeexpired;
          }
          $row['status'] = report_learnbook_usermodel::reportstatus($row);

          // Clean Up

          unset($row->id);
          unset($row['id']);
          unset($row['courseid']);
          unset($row['coursevisible']);
          unset($row['gradeitemtype']);
          // unset($row['gradefinalgrade']);
          unset($row['expirystatus-expiryexpirydate']);
          unset($row['gradefinalgrade-gradefinaltimemodified']);
          unset($row['f2f-status-timestart-timefinish']);
          unset($row['completionstate-completiontimemodified']);
          unset($row['gradeiteminstance']);
          unset($row['coursemoduleid']);
          unset($row['gradeitemid']);

          if (!$madeHeader) {
            $headers = array_keys($row);
            fputcsv($fp, $headers, ',', '"');
            $madeHeader = true;
          }
          fputcsv($fp, $row, ',', '"');
        }

        fclose($fp);

        return $filename;

      } else { // Create JSON Model.

        foreach ($x as $row) {

          if (!isset($userRecords[$row->userid])) {
            $userRecords[$row->userid] = new stdClass();
            $userRecords[$row->userid]->id = (int) $row->userid;
            $userRecords[$row->userid]->profile = self::fetchUserDetails($row->userid); // Keep this... Model is okay to be slow..
            $userRecords[$row->userid]->courses = array();
          }

          if(!isset($userRecords[$row->userid]->courses[$row->courseid])) {
            $userRecords[$row->userid]->courses[$row->courseid] = new stdClass();
            $userRecords[$row->userid]->courses[$row->courseid]->id = (int) $row->courseid;
            $userRecords[$row->userid]->courses[$row->courseid]->fullName = (string) $row->coursefullname;
            $userRecords[$row->userid]->courses[$row->courseid]->shortName = (string) $row->courseshortname;
            $userRecords[$row->userid]->courses[$row->courseid]->visible = (bool) $row->coursevisible;
            $userRecords[$row->userid]->courses[$row->courseid]->results = array();
          }

          if (!isset($userRecords[$row->userid]->courses[$row->courseid]->certificate)) {
            $userRecords[$row->userid]->courses[$row->courseid]->certificate = self::courseCertificateStatus($row->userid, $row->courseid);
          }

          if(!isset($userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid])) {
            $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid] = new stdClass();
            $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->id = (int) $row->gradeitemid;
            if ($row->gradeitemtype != 'course') {
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->itemName = (string) $row->gradeitemname;
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->idNumber = (string) $row->gradeitemidnumber;
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->itemModule = (string) $row->gradeitemmodule;
            } else {
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->itemName = null;
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->idNumber = null;
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->itemModule = null;
            }
            $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->itemType = (string) $row->gradeitemtype;
            
            if ($row->gradeitemmodule == 'facetoface') {

              if ($row->{'f2f-status-timestart-timefinish'}) {
                  $f2fstatus = @explode(',', $row->{'f2f-status-timestart-timefinish'});
                  $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->facetofaceState = facetoface_get_status($f2fstatus[0]);
                  $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->facetofacesessionstart = $f2fstatus[1];
                  $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->facetofacesessionend = $f2fstatus[2];
                } else {
                  $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->facetofaceState = null;
                  $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->facetofacesessionstart = null;
                  $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->facetofacesessionend = null;
                }

            }

            $completion = @explode(',', $row->{'completionstate-completiontimemodified'});
            $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->completionState = (bool) @$completion[0];
            $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->completionTimeModified = (int) @$completion[1];

            if ($useRecurring) {

              $expiry = @explode(',', $row->{'expirystatus-expiryexpirydate'});

              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->expirystatus = (bool) @$expiry[0];
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->expirydate = (int) @$expiry[1];

              if (!isset($userRecords[$row->userid]->courses[$row->courseid]->expiry)) {
                $userRecords[$row->userid]->courses[$row->courseid]->expiry = new stdClass();
                $userRecords[$row->userid]->courses[$row->courseid]->expiry->expirystatus = (bool) @$expiry[0];
                $userRecords[$row->userid]->courses[$row->courseid]->expiry->expirydate = (int) @$expiry[1];
              }

            }

            $graded = @explode(',', $row->{'gradefinalgrade-gradefinaltimemodified'});

            if (!@$graded[0]) {
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->gradeFinalGrade = null;
            } else {
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->gradeFinalGrade = (float) @$graded[0];
            }
            
            $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->gradeTimeModified = (int) @$graded[1];

            $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->CourseModuleID = (int) $row->coursemoduleid;

            if ($row->gradeitemtype == 'mod') {
              $userRecords[$row->userid]->courses[$row->courseid]->results[$row->gradeitemid]->isAccessible = self::isCourseModuleAccessible($row->userid, $row->courseid, $row->gradeitemmodule, $row->gradeiteminstance);
            }

          }

          if ($row->gradeitemtype == 'course') {
            $userRecords[$row->userid]->courses[$row->courseid]->completionstate = (bool) @$completion[0];
            $userRecords[$row->userid]->courses[$row->courseid]->completiontimemodified = (int) @$completion[1];
          }

        }

      }

      $x->close();

      // ob_get_clean();

      // echo '<pre>';
      // die(json_encode($userRecords));

      return $userRecords;
    }

    public static function visibleEnrolledCourseCount($userid) {

      global $DB;
      $count = 0;
      $rcs = $DB->get_records_sql("SELECT DISTINCT(C.id) 
                                      FROM {user_enrolments} UE, {enrol} E, {course} C, {course_completion_criteria} CCC
                                      WHERE E.id = UE.enrolid 
                                      AND E.status = 0 
                                      AND E.courseid = C.id 
                                      AND C.visible = 1
                                      AND C.id = CCC.course
                                      AND UE.userid = ?",
                                    array($userid)
                                    );
      // echo '<pre>';
      foreach ($rcs as $rc) {
        // echo var_dump($rc);
        $count++;
      }
      return($count);
    }

    public static function visibleEnrolledCourseCompletions($userid) {
      global $DB;
      $count = 0;
      $rcs = $DB->get_records_sql("SELECT DISTINCT(C.id), CC.timecompleted
                                      FROM {user_enrolments} UE, {enrol} E, {course} C, {course_completions} CC, {course_completion_criteria} CCC
                                      WHERE E.id = UE.enrolid 
                                      AND E.status = 0 
                                      AND E.courseid = C.id
                                      AND C.id = CCC.course                    
                                      AND C.visible = 1
                                      AND CC.course = C.id
                                      AND CC.userid = UE.userid
                                      AND UE.userid = ?",
                                      array($userid)
                                      );
      foreach ($rcs as $rc) {
        if($rc->timecompleted != NULL){
          // echo var_dump($rc);
          $count++;
        }
      }
      // die();
      return($count);
    }

    public static function fetchUserSqlString($users) {
        $firstUser = true;
        $userString = "(";
        $userString .= $users[0];

        foreach ($users as $user) {
            if($firstUser) {
                $firstUser = false;
                continue;
            }
            $userString .= "," . $user;
        }
        $userString .= ")";
      return $userString;
    }


    public static function fetchChildUsers($childString) {
      global $DB;
      $childrenProfiles = $DB->get_records_sql("SELECT U.id, U.lastlogin, U.firstname, U.lastname, U.department
                                                    FROM {user} U
                                                    WHERE U.suspended = 0
                                                    AND U.deleted = 0
                                                    AND id IN " . $childString
                                                    );
      return $childrenProfiles;
    }
  }
