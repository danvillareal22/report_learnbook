<?php
$string['eventcomponent'] = 'Component';
$string['eventcontext'] = 'Event context';
$string['eventloggedas'] = '{$a->realusername} as {$a->asusername}';
$string['eventorigin'] = 'Origin';
$string['eventrelatedfullnameuser'] = 'Affected user';
$string['eventreportviewed'] = 'Log report viewed';
$string['eventuserreportviewed'] = 'User log report viewed';
$string['log:view'] = 'View course logs';
$string['log:viewtoday'] = 'View today\'s logs';
$string['page'] = 'Page {$a}';
$string['logsformat'] = 'Logs format';
$string['nologreaderenabled'] = 'No log reader enabled';
$string['page-report-log-x'] = 'Any log report';
$string['page-report-log-index'] = 'Course log report';
$string['page-report-log-user'] = 'User course log report';
$string['pluginname'] = 'Learnbook Report';
$string['pluginname_desc'] = 'This is a custom reporting from Learnbook.';
$string['selectlogreader'] = 'Select log reader';



$string['reportfields'] = 'Report Fields';
$string['reportfields_desc'] = 'Comma Separated Values for what Fields should be available in Learnbook Reporting.';

$string['grade'] = 'Include Grade';
$string['grade_desc'] = 'This will include course and activity grade on the report';

$string['dategrade'] = 'Include Date Graded';
$string['dategrade_desc'] = 'This will include course and activity date grade on the report';

$string['disablerecurring'] = 'Disable Recurring';
$string['disablerecurring_desc'] = 'Flag to disable the Expired Recurring Training Column from Learnbook Reporting. If Recurring Training is not installed the system will silently disable it.';

$string['enablescheduling'] = 'Enable Scheduling';
$string['enablescheduling_desc'] = 'Flag to enable or disable Scheduling and emailing of reports. Requires local cronjobs to be configured.';

$string['enablelearnbooktranscript'] = 'Enable Learnbook Transcript';
$string['enablelearnbooktranscript_desc'] = 'Flag to enable or disable the Learnbook Transcript.';

$string['enablecustomscheduling'] = 'Enable Custom Report Schedules';
$string['enablecustomscheduling_desc'] = 'Flag to enable custom report schedules other than now.';

$string['enableactivitylevelfiltering'] = 'Enable Activity Level Filtering';
$string['enableactivitylevelfiltering_desc'] = 'Flag to enable Activity Level Filtering.';

$string['usefontawesome'] = 'Enable Font Awesome';
$string['usefontawesome_desc'] = 'Flag to enable Font Awesome.';

$string['userealcounts'] = 'Enable Actual Counts';
$string['userealcounts_desc'] = 'Flag to enable Actual Counts - Disable if Reports are slow.';

$string['enableadvancedfilters'] = 'Enable Advanced Filtering Module';
$string['enableadvancedfilters_desc'] = 'Enable Advanced Filtering Module';

$string['emailsubject'] = 'Email Subject';
$string['emailsubject_desc'] = 'Email Subject';

$string['emailbody'] = 'Email Body';
$string['emailbody_desc'] = 'Email Body';

$string['cohortmanagershortname'] = 'cohortmanager';

$string['loginasuserbutton'] = 'Login as this user';
$string['notenrolledinanycourse'] = 'You are not enrolled in any courses.';
$string['nomembersincohort'] = 'There are no team members assigned to you';

$string['dashboardcourse'] = 'Course';
$string['dashboarditem'] = 'Activity / Grade Item';
$string['dashboarddate'] = 'Date of Completion';
$string['dashboardcomplete'] = 'Complete';
$string['dashboardgrade'] = 'Grade';
$string['dashboardcert'] = 'Certificate';