<?php

namespace report_learnbook\task;

class process_end_user_notifications extends \core\task\scheduled_task {  

    public function get_name() {
        // Shown in admin screens
        return 'Process End User Email Notifications';
    }
                                                                     
    public function execute() {
    	global $CFG;
        require_once($CFG->dirroot . '/report/learnbook/lib.php');
        \report_learnbook_report::processEndUserNotifications();
    }                                                                                                                               
} 