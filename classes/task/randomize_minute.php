<?php

namespace report_learnbook\task;

class randomize_minute extends \core\task\scheduled_task {    

    public function get_name() {
        return 'Randomise Report Processing Minute';
    }
                                                                     
    public function execute() {
    	$task = \core\task\manager::get_scheduled_task('report_learnbook\task\process_reports');
    	$defaulttask = \core\task\manager::get_default_scheduled_task('report_learnbook\task\process_reports');  
        $task->set_minute($defaulttask->get_minute());
        $task->set_hour($defaulttask->get_hour());
        $task->set_month($defaulttask->get_month());
        $task->set_day_of_week($defaulttask->get_day_of_week());
        $task->set_day($defaulttask->get_day());
        $task->set_disabled($defaulttask->get_disabled());
        $task->set_customised(false);
        \core\task\manager::configure_scheduled_task($task);
    	return true;
    }                                                                                                                               
}