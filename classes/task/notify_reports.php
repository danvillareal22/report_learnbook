<?php

namespace report_learnbook\task;

class notify_reports extends \core\task\scheduled_task {      
    public function get_name() {
        return 'Notify Users of Processed Reports';
    }
                                                                     
    public function execute() {
    	global $CFG;
        require_once($CFG->dirroot . '/report/learnbook/lib.php');
        \report_learnbook_report::notifyReports();
    }
                                                                                                                              
}