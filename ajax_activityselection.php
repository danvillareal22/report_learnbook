<?php

require_once('../../config.php');

$filter = optional_param('q', '', PARAM_TEXT);

if ($filter) {
	$filter = $filter . '%';
}

$selectedcourseids = optional_param_array('courseids', array(), PARAM_INT);

$showAllCourses = in_array('-1', $selectedcourseids);

if ($showAllCourses) {
	$selectedcourseids = array();
}

require_once($CFG->dirroot . '/report/learnbook/lib.php');

$data = report_learnbook_report::constructGradeItemSelector($selectedcourseids, $filter);

$output = new stdClass();
$output->totalcount = count($data);
$output->incomplete_results = false;
$output->items = array();

foreach ($data as $itemid => $itemname) {
	$item = new stdClass();
	$item->id = (int) $itemid; // Grade Item ID
	$item->text = $itemname;
	$output->items[] = $item;
}

echo json_encode($output);