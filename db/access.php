<?php
defined('MOODLE_INTERNAL') || die();

$capabilities = array(
    'report/learnbook_globalreport:view' => array(
	    'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes'   => array(
	            'manager' => CAP_ALLOW,
				'editingteacher' => CAP_ALLOW,
				'teacher' => CAP_ALLOW
    	)
	),
	'report/learnbook_globalreport:viewallcohorts' => array(
	    'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes'   => array(
	            'manager' => CAP_ALLOW,
				'editingteacher' => CAP_ALLOW,
				'teacher' => CAP_ALLOW
    	)
	)
);


