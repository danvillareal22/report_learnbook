<?php

$tasks = array(                                                                                                                     
    array(                                                                                                                          
        'classname' => 'report_learnbook\task\notify_reports',                                                                            
        'blocking' => 0,                                                                                                            
        'minute' => '*',                                                                                                            
        'hour' => '*',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                         
    ),
    array(                                                                                                                          
        'classname' => 'report_learnbook\task\process_end_user_notifications',                                                                            
        'blocking' => 1,                                                                                                            
        'minute' => '*',                                                                                                            
        'hour' => '*',                                                                                                              
        'day' => '*',                                                                                                       
        'dayofweek' => '*',                                                                                                  
        'month' => '*'                                                                                                              
    ),
    array(                                                                                                                          
        'classname' => 'report_learnbook\task\process_reports',                                                                            
        'blocking' => 0,                                                                                                            
        'minute' => '*',                                                                                                            
        'hour' => '*',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                              
    ),
	array(                                                                                                                          
        'classname' => 'report_learnbook\task\randomize_minute',                                                                            
        'blocking' => 1,                                                                                                            
        'minute' => '2',                                                                                                            
        'hour' => '1',                                                                                                              
        'day' => '*',                                                                                                       
        'dayofweek' => '*',                                                                                                  
        'month' => '*'                                                                                                              
    ),
);