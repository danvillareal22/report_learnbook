<?php

require_once('../../config.php');

require_login();

$id = required_param('id', PARAM_INT);

$report = $DB->get_record_sql("SELECT * FROM {report_learnbook} WHERE id = ? AND filename IS NOT NULL", array($id));

if ($report) {

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($report->filename));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    // header('Content-Length: ' . filesize($file));
    
    $fs = get_file_storage();

	$systemcontext = context_system::instance();

	// Prepare file record object
	$fileinfo = array(
	    'contextid' => $systemcontext->id, // ID of context
	    'component' => 'report_learnbook',     // usually = table name
	    'filearea' => 'scheduled_reports',     // usually = table name
	    'itemid' => $id,               // usually = ID of row in table
	    'filepath' => '/',           // any path beginning and ending in /
	    'filename' => $report->filename); // any filename

	$file = $fs->get_file($fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'],
	                      $fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);

	// Read contents
	if ($file) {
	    $contents = $file->get_content();
	    echo $contents;
	} else {
	    print_error('Could not find Report');
	}

	exit;

} else {
	print_error('Could not find Report');	
}

die('here');