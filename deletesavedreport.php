<?php

require_once('../../config.php');

require_login();

$id = required_param('id', PARAM_INT);

$deleted = $DB->delete_records('report_learnbook', array('id' => $id));

if ($deleted) {
	redirect($CFG->wwwroot . '/report/learnbook/index.php', 'This report has been deleted.');
} else {
	redirect($CFG->wwwroot . '/report/learnbook/index.php', 'There was an issue deleting this report. Please try again later.');
}