<?php

require_once($CFG->dirroot . '/report/learnbook/vendor/autoload.php');
require_once($CFG->dirroot . '/report/learnbook/usermodel.php');
use Handlebars\Handlebars;

class serverreport {

	public static function fetchColumns() {
		global $DB;

		$learnbookconfig = get_config('report_learnbook');
		$headers = array();
		if (isset($learnbookconfig->reportfields)) {
			$fields = explode(',', $learnbookconfig->reportfields);

			foreach ($fields as $extrafield) {

				$extrafield = trim($extrafield);

				if (empty($extrafield)) {
					continue;
				}
				if($extrafield == 'userid'){
					$headers[] = 'User ID';
				} else if ($extrafield == 'username'){
					$headers[] = 'Username';
				} else if ($extrafield == 'fullname'){
					$headers[] = 'Full Name';
			    } else if ($extrafield == 'email'){
					$headers[] = 'Email Address';
				}else if($extrafield == 'coursecategory'){
					$headers[] = 'Course Category';
				} else if (substr($extrafield, 0, 14) == 'profile_field_') {
					$shortname = substr($extrafield, 14);
					$field = $DB->get_record_sql("SELECT name FROM {user_info_field} UIF WHERE UIF.shortname = ?", array($shortname));
					$headers[] = $field->name;
				} else if ($extrafield == 'earliestenrolment') {
					$headers[] = 'Enrolment Date';
				} else if ($extrafield == 'scorminteractiontime') {
					$headers[] = 'SCORM Playtime';
				} else if ($extrafield == 'cohorts') {
					$headers[] = 'Cohorts';
				} else if ($extrafield == 'cmhours') {
					$headers[] = 'CPD Hours';
				} else {
					if ($extrafield == 'timecreated') {
						$headers[] = 'User Created';
					} else {
						$headers[] = get_string($extrafield);
					}
				}
			}
		}

		$headers[] = 'Course';
		$headers[] = 'Activity';
		if($learnbookconfig->grade){
			$headers[] = 'Grade';
		}
		$headers[] = 'Status';
		if($learnbookconfig->dategrade){
			$headers[] = 'Date Graded';
		}
		if (report_learnbook_usermodel::useRecurring()) {
			$headers[] = 'Expiry Date';
		}
		
		return $headers;
	}

	public static function renderHTMLHeaders() {
		$headers = self::fetchColumns();
		ob_start();
		foreach ($headers as $header) {
			echo '<th>' . $header . '</th>';
		}
		return ob_get_clean();
	}

	public static function renderDataTable() {
		ob_start();
?>
		<table id="report_learnbook_reportserverside" class="display table table-striped table-responsive" cellspacing="0" width="100%">
	    	<thead>
	        	<tr>
	        		<?php echo self::renderHTMLHeaders(); ?>
	        	</tr>
	    	</thead>
	    	<tfoot>
	    		<tr>
	    			<?php echo self::renderHTMLHeaders(); ?>
	    		</tr>
	    	</tfoot>
		</table>
<?php
		return ob_get_clean();
	}

}

class report_learnbook_report {

	public static function form_cohort_multiselect() {
		global $USER;
		$cohorts = report_learnbook_report::fetchAccessibleCohorts($USER->id);

		foreach ($cohorts as $cohortId => $cohortName) {
			if($cohortId!=-1){
				if (strpos($cohortName, 'All Users') !== false) {
					unset($cohorts[$cohortId]);
				}
				
			}			
		}

		$selectedcohorts = optional_param_array('cohorts', array(), PARAM_INT);

		$userCanAccessAllCohorts = has_capability('report/learnbook_globalreport:viewallcohorts', context_system::instance(), $USER->id);
		if (!$userCanAccessAllCohorts) {
			if (empty($selectedcohorts)) {
				// $selected = $cohorts[];

				$selectedcohorts[] = array_keys($cohorts)[0];
			}
		}

		return html_writer::select($cohorts, 'cohorts[]', $selectedcohorts,false, array('multiple' => 'true', 'id' => 'learnbook_report_cohorts'));
	}

	public static function constructGradeItemSelector($selectedcourseids = array(), $filter = '') {
	
	global $CFG, $DB;

	$selectedscope = '';

	$qparams = array();

	if (!empty($selectedcourseids)) {
		$implode = implode(',', $selectedcourseids);
		$selectedscope = ' AND C.id IN (' . $implode . ') ';
	}

	if (!empty($filter)) {
		// die(print_r($filter));
		$selectedscope .= ' AND ((C.fullname LIKE ?) OR (GI.itemname LIKE ?) OR (C.shortname LIKE ?) OR (C.idnumber LIKE ?))';
		$qparams[] = $filter;
		$qparams[] = $filter;
		$qparams[] = $filter;
		$qparams[] = $filter;
	}

	$data = array();

	$query = "SELECT GI.*, C.fullname, C.category, C.idnumber as 'courseidnumber'
											FROM {grade_items} GI, {course} C
											WHERE GI.courseid = C.id AND GI.hidden = 0
											$selectedscope
											ORDER BY C.fullname ASC";

	$gradeitems = $DB->get_records_sql($query, $qparams);

 	$coursegradeitems = array();
 	require_once($CFG->dirroot.'/lib/coursecatlib.php');
	$arrCategories = coursecat::make_categories_list();

 	foreach ($gradeitems as $gradeitem) {

 		$catname = $gradeitem->fullname;

 		if (isset($arrCategories[$gradeitem->category])) {
 			$catname = $arrCategories[$gradeitem->category] . ' / ' . $gradeitem->fullname;
 		}
 		
 		if (!isset($coursegradeitems[$catname])) {
 			$coursegradeitems[$catname] = array();
 		}
 		$coursegradeitems[$catname][] = $gradeitem;
 	}
 	
 	foreach ($coursegradeitems as $coursename => $gi) {

	 		// if (!isset($data[$coursename])) {
	 		// 	$data[$coursename] = array();
	 		// }

	 		foreach ($gi as $gradeitem) {
	 			$name = '';
	 			if (!empty($gradeitem->idnumber)) {
	 				$name .= '[' .$gradeitem->idnumber . '] ';
	 			}
	 			if ($gradeitem->itemtype == 'course') {

	 				if (!empty($gradeitem->courseidnumber)) {
	 					$name .= '[' . $gradeitem->courseidnumber . ']' . $gradeitem->fullname . ' (Course Grade)';
	 				} else {
	 					$name .= $gradeitem->fullname . ' (Course Grade)';
	 				}
	 				
	 			} else {
	 				if (!empty($gradeitem->itemname)) {
	 					$name .= $gradeitem->itemname . ' ';
	 				}
	 				if ($gradeitem->itemtype == 'manual') {
	 					$name .= '(Barcode Scan)';
	 				} else if ($gradeitem->itemmodule == 'scorm') {
	 					$name .= '(eLearning)';
	 				}
	 			}

	 			$data[$gradeitem->id] = $name . ' - ' . $coursename;
	 		}

	 	}
		return $data;
	}

	public static function processReport($string, $taskid) {
		global $CFG;

		$params = array();

		parse_str($string, $params);

		$daterange = explode(' - ', @$params['daterange']);

		$startdate = false;
		$enddate = false;

		if ($daterange) {
			$startdate = strtotime($daterange[0]);
			if (isset($daterange[1])) {
				$enddate = new DateTime();
				$enddate->setTimestamp(strtotime($daterange[1]));
				$enddate->modify('tomorrow');
				$enddate = $enddate->getTimestamp();
			} else {
				$enddate = new DateTime();
				$enddate->setTimestamp($startdate);
				$enddate->modify('tomorrow');
				$enddate = $enddate->getTimestamp();
			}
		}

		$cohortids = $params['cohorts'];
		$courseids = $params['courses'];

		$showAllCohorts = in_array('-1', $cohortids);
		$showAllCourses = in_array('-1', $courseids);

		if ($showAllCohorts) {
			$userids = array();
		} else {
			$userids = report_learnbook_report::fetchUsersInCohorts($cohortids, @$options['excludesuspended']);
		}

		if ($showAllCourses) {
			$courseids = array();
		}

		$options = array();
		$options['excludesuspended'] = $params['excludesuspended'];
		$options['startdate'] = $startdate;
		$options['enddate'] = $enddate;
		$options['status'] = $params['status'];
		$options['limit'] = 500000;
		$options['flat'] = true;

		$filepath = report_learnbook_usermodel::fetchUsers($userids, $courseids, $options, $params);

		$fs = get_file_storage();

		$systemcontext = context_system::instance();
 
		// Prepare file record object
		$fileinfo = array(
		    'contextid' => $systemcontext->id, // ID of context
		    'component' => 'report_learnbook',     // usually = table name
		    'filearea' => 'scheduled_reports',     // usually = table name
		    'itemid' => $taskid,               // usually = ID of row in table
		    'filepath' => '/',           // any path beginning and ending in /
		    'filename' => $filepath); // any filename

		$status = $fs->create_file_from_pathname($fileinfo, $CFG->dataroot . '/' . $filepath);

		unlink($CFG->dataroot . '/' . $filepath);

		return $filepath;
	}

	public static function notifyReports() {

		global $DB, $CFG;

		$notifications = $DB->get_records_sql("SELECT * FROM {report_learnbook} WHERE mailed IS NULL AND processed IS NOT NULL and filename IS NOT NULL");

		$from = core_user::get_support_user();

		$course = $DB->get_record_sql("SELECT * FROM {course} WHERE id = 1");

		foreach ($notifications as $notification) {

			$params = array();

			parse_str($notification->params, $params);



			$user = $DB->get_record_sql("SELECT * FROM {user} WHERE id = ?", array($notification->userid));

			if (!$user) {
				mtrace('Unable to Identify User! Skipping Notification.');
				continue;
			}

			$link = $CFG->wwwroot . '/report/learnbook/downloadsavedreport.php?id=' . $notification->id;

			$subject = '[' . $course->fullname . ' LMS] Your saved report is ready for download!';
			$message = "Hi $user->firstname $user->lastname,\r\n\r\nYou are receiving this email because you have saved a report.\r\n\r\nThe report can be downloaded by following this link:\r\n\r\n$link\r\n\r\nThis is an automated message sent from the $course->fullname LMS.";

			// Add an if statement to check if email has been updated
			// $user->email = $params["email"]
			$user->email = $params["email"];
			email_to_user($user, $from, $subject, $message);
			$notification->mailed = time();
			$update = $DB->update_record('report_learnbook', $notification);

		}

	}

	public static function processSchedule() {
		global $DB;
		$scheduledReports = $DB->get_records_sql("SELECT * FROM {report_learnbook}");
		
		if(isset($scheduledReports)){
			foreach ($scheduledReports as $task) {		
				$processingSchedule = strtotime($task->schedule); // Start of Day

				$minProcessingSchedule = strtotime("midnight", $processingSchedule);
				// $minProcessingSchedule = $processingSchedule;

				$maxProcessingSchedule = new DateTime();
				$maxProcessingSchedule->setTimestamp($minProcessingSchedule);
				$maxProcessingSchedule->modify('+1 day');
				$maxProcessingSchedule = $maxProcessingSchedule->getTimestamp();

					$runTime = time();

						echo '#####REPORT#####';
						echo "Report Schedule: $task->schedule\n";
						// echo "Report Schedule Output: $minProcessingSchedule\n";
						echo "Report Run Time: " . $runTime . " " . gmdate('Y-m-d\TH:i:s\Z', $runTime) . "\n";
						echo "Report Schedule Min: " . $minProcessingSchedule . " " . gmdate('Y-m-d\TH:i:s\Z', $minProcessingSchedule) . "\n";
						echo "Report Schedule Max: " . $maxProcessingSchedule . " " . gmdate('Y-m-d\TH:i:s\Z', $maxProcessingSchedule) . "\n";
					
					// if (($minProcessingSchedule <= time()) && ($maxProcessingSchedule >= time()) && (date('m-d-Y',$task->processed) != date('m-d-Y', $minProcessingSchedule) && $task->schedule != 'now')) {
					if (($runTime >= $minProcessingSchedule) && ($runTime <= $maxProcessingSchedule) && ($task->processed <= $minProcessingSchedule) && ($task->schedule != 'now')) {
						echo "Should this report run? Yes \n";
						echo "Has this report been processed today? processed\n";
						$filepath = self::processReport($task->params, $task->id);
						$task->filename = $filepath;
						$task->processed = $runTime;
						$task->mailed = NULL;
						$DB->update_record('report_learnbook', $task);
						
					}elseif(($task->schedule == 'now') && ($task->processed == NULL)){
						echo "Should this report run NOW? Yes \n";
						echo "Has this report been processed NOW today? processed\n";
						$filepath = self::processReport($task->params, $task->id);
						$task->filename = $filepath;
						$task->processed = time();
						$task->mailed = NULL;
						$DB->update_record('report_learnbook', $task);
					}
					else {
						echo "Should this report run? No \n";
						echo "Has this report been processed today? Not processed\n";
					}		
			}
		}
	}
	
	public static function fetchAccessibleCohorts($userid) {
		global $DB;
		$options = array();
		$userCanAccessAllCohorts = has_capability('report/learnbook_globalreport:viewallcohorts', context_system::instance(), $userid);
		if ($userCanAccessAllCohorts) {
			$cohorts = $DB->get_records_sql("SELECT C.id, C.name, (SELECT COUNT(id) FROM {cohort_members} CM WHERE CM.cohortid = C.id) as 'members' FROM {cohort} C ORDER BY C.name ASC");
			$options = array();
			$counts = $DB->get_record_sql("SELECT COUNT(id) as 'total', SUM(suspended) as 'suspended' FROM {user} U WHERE U.deleted = 0");
			
			$excludesuspended = optional_param('excludesuspended', 1, PARAM_INT);
			$tagline = '';

			if ($excludesuspended) {
				$tagline = '(' . ($counts->total - $counts->suspended) . '/' . $counts->total . ')';
			} else {
				$tagline = '(' . $counts->total . '/' . $counts->total . ')';
			}

			$options[-1] = 'All Users '. $tagline;

			foreach ($cohorts as $cohort) {
				$options[$cohort->id] = $cohort->name . ' (' . $cohort->members . ')';
			}

			return $options;
		} else {
			$assignedCohorts = $DB->get_records_sql("SELECT UID.data FROM {user_info_data} UID, {user_info_field} UIF WHERE UID.fieldid = UIF.id AND UIF.datatype = 'cohortselect' AND UID.userid = ?", array($userid));
			foreach ($assignedCohorts as $ac) { // Just use the first for now.
				$ac = explode(',', $ac->data);
				foreach ($ac as $v) {
					$cohort = $DB->get_record_sql("SELECT C.id, C.name, (SELECT COUNT(id) FROM {cohort_members} CM WHERE CM.cohortid = C.id) as 'members' FROM {cohort} C WHERE C.id = ? ORDER BY C.name ASC", array($v));
					if ($cohort) {
						$options[$cohort->id] = $cohort->name . '(' . $cohort->members . ')';
					}
				}
			}
		}

		return $options;
	}

	public static function form_course_multiselect() {
		global $USER;
		$courses = report_learnbook_report::fetchAccessibleCourses($USER->id);
		$selectedcourses = optional_param_array('courses', array(), PARAM_INT);

		$userCanAccessAllCohorts = has_capability('report/learnbook_globalreport:viewallcohorts', context_system::instance(), $USER->id);
		if (!$userCanAccessAllCohorts) {
			if (empty($selectedcourses)) {
				$limit = 1;
				$iteration = 0;
				foreach ($courses as $courseid => $course) {
					if ($iteration >= $limit) {
						break;
					}
					$selectedcourses[] = $courseid;
					$iteration++;
				}
			}
		}

		return html_writer::select($courses, 'courses[]', $selectedcourses,false, array('multiple' => 'true', 'id' => 'learnbook_report_courses'));
	}

	public static function form_course_suspendedusers() {
		$excludesuspended = optional_param('excludesuspended', 1, PARAM_INT);
		return '<input type="hidden" value="0" name="excludesuspended" />' . html_writer::checkbox('excludesuspended',"1", $excludesuspended,'', array('id' => 'learnbook_report_excludesuspended'));
	}

	public static function fetchAccessibleCourses($userid) {

		global $CFG, $DB;

		$options = array();

		require_once($CFG->dirroot.'/lib/coursecatlib.php');
		$arrCategories = coursecat::make_categories_list();

		$userCanAccessAllCohorts = has_capability('report/learnbook_globalreport:viewallcohorts', context_system::instance(), $userid);
		if ($userCanAccessAllCohorts) {
			$courses = $DB->get_records_sql('SELECT * FROM {course} WHERE id NOT IN (1) ORDER BY shortname, fullname ASC');
			$count = count($courses);
			$options[-1] = 'All Courses (' . $count . ')';
			foreach ($courses as $course) {
				$options[$course->id] = $arrCategories[$course->category] . ' / ' . $course->fullname;
			}
		} else {

			$accessibleCohorts = self::fetchAccessibleCohorts($userid);
			$accessibleCohorts = array_keys($accessibleCohorts);
			$accessibleCohorts = implode(',', $accessibleCohorts);

			// $courses = $DB->get_records_sql("SELECT C.id, C.fullname FROM {enrol} E, {course} C WHERE C.id = E.courseid AND E.enrol = 'cohort' AND E.status = 0 AND customint1 IN ($accessibleCohorts)");
			$courses = $DB->get_records_sql("SELECT C.id, C.fullname, C.shortname FROM {course} C WHERE id NOT IN (1) ORDER BY shortname, fullname ASC");

			foreach ($courses as $course) {
				$options[$course->id] = '(' . $course->shortname . ')' . $course->fullname;
			}
		}

		return $options;
	}

	public static function fetchUsersInCohorts($cohortids, $excludesuspended = true) {
		global $DB;

		$cohortidsstring = implode(',', $cohortids);

		if (empty($cohortidsstring)) {
			$usersInCohorts = array();
		} else {
			if ($excludesuspended) {
				$excludesuspended = (int)!$excludesuspended;
				$usersInCohorts = $DB->get_records_sql("SELECT CM.id, CM.userid FROM {cohort_members} CM, {user} U WHERE CM.userid = U.id AND U.suspended = ? AND CM.cohortid IN ($cohortidsstring)", array($excludesuspended));	
			} else {
				$usersInCohorts = $DB->get_records_sql("SELECT CM.id, CM.userid FROM {cohort_members} CM WHERE CM.cohortid IN ($cohortidsstring)");
			}
			
		}
		
		$userids = array();

		foreach ($usersInCohorts as $user) {
			$userids[] = $user->userid;
		}

		if (empty($userids)) {
			$userids[] = '-99';
		}

		return $userids;

	}

	public static function fetchData($cohortids, $courseids, $userid = false, $options) {

		global $CFG, $DB, $USER;

		require_once($CFG->dirroot . '/report/learnbook/usermodel.php');

		if ($userid) {
			$userCanAccessReport = has_capability('report/learnbook_globalreport:view', context_system::instance(), $userid);
			if (!$userCanAccessReport) {
				// return array();
			}
		}

		$showAllCohorts = in_array('-1', $cohortids);
		$showAllCourses = in_array('-1', $courseids);

		if ($showAllCohorts) {
			$userids = array();
		} else {
			$userids = self::fetchUsersInCohorts($cohortids, @$options['excludesuspended']);
		}

		if ($showAllCourses) {
			$courseids = array();
		}
		
		$data = report_learnbook_usermodel::fetchUsers($userids, $courseids, $options);
		gc_collect_cycles();
		return self::DataToRows($data);
	}

	public static function DataToRows(&$data) {
		$rows = array();
		foreach ($data as $key => $user) {
			foreach ($user->courses as $course) {
				foreach ($course->results as $result) {
					$row = array();
					$row['userid'] = $user->id;
					$row['suspended'] = $user->profile->suspended;
					$row['username'] = $user->profile->username;
					$row['firstname'] = $user->profile->firstname;
					$row['lastname'] = $user->profile->lastname;
					$row['email'] = $user->profile->email;
					foreach ($course as $key => $value) {
						if (is_object($value)) {
							continue;
						}
						if (is_array($value)) {
							continue;
						}
						$row['course_' . $key] = $value;
					}
					$row['courseid'] = $course->id;
					$row['coursefullname'] = $course->fullName;
					foreach ($result as $key => $value) {
						$row['activity_' . $key] = $value;
					}
					foreach ($user->profile as $key => $value) {
						if (is_object($value)) {
							continue;
						}
						if (is_array($value)) {
							continue;
						}
						$row['userprofile_' . $key] = $value;
					}
					$rows[] = $row;
				}
			}
			unset($data[$key]);
			gc_collect_cycles();
		}
		return $rows;
	}

	public static function fetchNotificationData($string, $limit = 500000) {
		$params = array();
		parse_str($string, $params);
		$daterange = explode(' - ', @$params['daterange']);
		$startdate = false;
		$enddate = false;
		if ($daterange) {
			$startdate = strtotime($daterange[0]);
			if (isset($daterange[1])) {
				$enddate = new DateTime();
				$enddate->setTimestamp(strtotime($daterange[1]));
				$enddate->modify('tomorrow');
				$enddate = $enddate->getTimestamp();
			} else {
				$enddate = new DateTime();
				$enddate->setTimestamp($startdate);
				$enddate->modify('tomorrow');
				$enddate = $enddate->getTimestamp();
			}
		}
		$cohortids = $params['cohorts'];
		$courseids = $params['courses'];
		$showAllCohorts = in_array('-1', $cohortids);
		$showAllCourses = in_array('-1', $courseids);
		if ($showAllCohorts) {
			$userids = array();
		} else {
			$userids = report_learnbook_report::fetchUsersInCohorts($cohortids, @$options['excludesuspended']);
		}
		if ($showAllCourses) {
			$courseids = array();
		}
		$options = array();
		$options['excludesuspended'] = $params['excludesuspended'];
		$options['startdate'] = $startdate;
		$options['enddate'] = $enddate;
		$options['status'] = $params['status'];
		$options['limit'] = $limit;
		$data = report_learnbook_usermodel::fetchUsers($userids, $courseids, $options, $params);
		return $data;
	}

	public static function processEndUserNotifications() {

		global $CFG, $DB, $SITE;

		require_once($CFG->dirroot . '/user/lib.php');

		$starttimestamp = new DateTime();
		$starttimestamp->modify('midnight');
		$starttimestamp = $starttimestamp->format('U');

		$endtimestamp = new DateTime();
		$endtimestamp->modify('midnight');
		$endtimestamp->modify('+1 day');
		$endtimestamp = $endtimestamp->format('U');

		$endUserNotifications = $DB->get_records_sql("SELECT * FROM {report_lb_notifications} WHERE active = 1 AND maxsend != 0 AND startdate <= ? AND enddate >= ?", array($starttimestamp, $endtimestamp));

		if(isset($endUserNotifications)) {

			foreach ($endUserNotifications as $notification) { 

				$processingSchedule = strtotime($notification->msgint); // Start of Day
				// $processingSchedule = strtotime('today'); // Start of Day

				$minProcessingSchedule = strtotime("midnight", $processingSchedule);
				$minProcessingSchedule = $processingSchedule;

				$maxProcessingSchedule = new DateTime();
				$maxProcessingSchedule->setTimestamp($minProcessingSchedule);
				$maxProcessingSchedule->modify('+1 day');
				$maxProcessingSchedule = $maxProcessingSchedule->getTimestamp();

				$runTime = time();

				echo '#####REPORT#####';
				echo "Report Schedule: $notification->interval\n";
				// echo "Report Schedule Output: $minProcessingSchedule\n";
				echo "Report Last Run Time: " . $notification->processed . " " . gmdate('Y-m-d\TH:i:s\Z', $notification->processed) . "\n";
				echo "Report Run Time: " . $runTime . " " . gmdate('Y-m-d\TH:i:s\Z', $runTime) . "\n";
				echo "Report Schedule Min: " . $minProcessingSchedule . " " . gmdate('Y-m-d\TH:i:s\Z', $minProcessingSchedule) . "\n";
				echo "Report Schedule Max: " . $maxProcessingSchedule . " " . gmdate('Y-m-d\TH:i:s\Z', $maxProcessingSchedule) . "\n";

				$dryrun = false;
				// $notification->processed = 0;
				if( ($notification->msgint == 'today') && ($notification->maxsend == 1) ){
					$notification->processed = 0;
				}

				$from = core_user::get_support_user();

				if (($runTime >= $minProcessingSchedule) && ($runTime <= $maxProcessingSchedule) && (($notification->processed < $minProcessingSchedule) OR ($notification->processed == null)) && ($notification->msgint != 'now')) {
					echo "Should this report run? Yes \n";
					echo "Has this report been processed today?\n";
					$data = report_learnbook_report::fetchNotificationData($notification->params);
					// mtrace('Fetched Data');
					// print_r($data);
					$template = $notification->body;

					$handlebars = new Handlebars();
					foreach ($data as $akey => $notificationdata) {

						$sendSeparateEmailsPerCourse = true;

						if ($sendSeparateEmailsPerCourse) {

							foreach ($notificationdata->courses as $course) {

								$notificationdata->site = new stdClass();
								$notificationdata->site->wwwroot = $CFG->wwwroot;
								$notificationdata->site->support_emailaddress = $from->email;
								$notificationdata->site->support_name = $from->firstname . ' ' . $from->lastname;
								$notificationdata->site->fullName = $SITE->fullname;
								$notificationdata->site->shortName = $SITE->shortname;

								$firstResult = array_values($course->results)[0];
								$notificationdata->course = $course;
								$notificationdata->activity = $firstResult;
								$emailnotification = $handlebars->render($template, $notificationdata);
								$emailnotification = $emailnotification;
			
								$user = $notificationdata->profile;
								$subject = $notification->subject;

								$subject = $handlebars->render($subject, $notificationdata);

								$lastSent = get_user_preferences('LN-LastSent-' . $notification->id, 0, $notificationdata->profile->id);
								$timesSent = get_user_preferences('LN-TimesSent-' . $notification->id, 0, $notificationdata->profile->id);

								// $lastSent = strtotime('yesterday');

								if (($timesSent >= $notification->maxsend) && ($notification->maxsend != -1) && ($notification->maxsend != -1)) {
										mtrace('TODO! Implement Log Not Send too many times');
								} else {
									if (($lastSent >= $minProcessingSchedule) && ($lastSent <= $maxProcessingSchedule) && ($notification->schedule != 'now')) {
										mtrace('TODO! Implement Log Not Sending multiple times a day');
										continue;
									} else {
										mtrace('TODO! Implement Log for sent Email!');

										if ($dryrun) {

											mtrace('Performing Dry Run.');
											print_r($user);
											print_r($from);
											print_r($subject);
											print_r($emailnotification);
											set_user_preference('LN-LastSent-' . $notification->id, time(), $notificationdata->profile->id);
											set_user_preference('LN-TimesSent-' . $notification->id, ($timesSent + 1), $notificationdata->profile->id);

										} else {

											mtrace('Sending for real.');

											print_r($user);
											print_r($from);
											print_r($subject);
											print_r($emailnotification);
											email_to_user($user, $from, $subject, $emailnotification);
											set_user_preference('LN-LastSent-' . $notification->id, time(), $notificationdata->profile->id);
											set_user_preference('LN-TimesSent-' . $notification->id, ($timesSent + 1), $notificationdata->profile->id);

										}
										
										
									}				
								}

							}

						} else {

							$notificationdata->site = new stdClass();
							$notificationdata->site->wwwroot = $CFG->wwwroot;
							$notificationdata->site->support_emailaddress = $from->email;
							$notificationdata->site->support_name = $from->firstname . ' ' . $from->lastname;
							$notificationdata->site->fullName = $SITE->fullname;
							$notificationdata->site->shortName = $SITE->shortname;

							$firstCourse = array_values($notificationdata->courses)[0];
							$firstResult = array_values($firstCourse->results)[0];
							$notificationdata->course = $firstCourse;
							$notificationdata->activity = $firstResult;
							$emailnotification = $handlebars->render($template, $notificationdata);
							$emailnotification = $emailnotification;
		
							$user = $notificationdata->profile;
							$subject = $notification->subject;

							$subject = $handlebars->render($subject, $notificationdata);

							$lastSent = get_user_preferences('LN-LastSent-' . $notification->id, 0, $notificationdata->profile->id);
							$timesSent = get_user_preferences('LN-TimesSent-' . $notification->id, 0, $notificationdata->profile->id);

							$lastSent = strtotime('yesterday');

							if (($timesSent >= $notification->maxsend) && ($notification->maxsend != -1) && ($notification->maxsend != -1)) {
									mtrace('TODO! Implement Log Not Send too many times');
							} else {
								if (($lastSent >= $minProcessingSchedule) && ($lastSent <= $maxProcessingSchedule) && ($notification->schedule != 'now')) {
									mtrace('TODO! Implement Log Not Sending multiple times a day');
									continue;
								} else {
									mtrace('TODO! Implement Log for sent Email!');
									email_to_user($user, $from, $subject, $emailnotification);
									set_user_preference('LN-LastSent-' . $notification->id, time(), $notificationdata->profile->id);
									set_user_preference('LN-TimesSent-' . $notification->id, ($timesSent + 1), $notificationdata->profile->id);
									
								}				
							}

						}

					}

					$processed = new stdClass();
					$processed->id = $notification->id;
					$processed->processed = time();
					$DB->update_record('report_lb_notifications', $processed);

					// Process This Scheduled Report

				}elseif(($notification->schedule == 'now') && ($notification->processed == NULL)){
					echo "Should this report run NOW? Yes \n";
					echo "Has this report been processed NOW today? processed\n";

					mtrace('TODO! Implement "Now" Notifications');
				}
				else {
					echo "Should this report run? No \n";
					echo "Has this report been processed today? Not processed\n";
				}		
			}
		}
	}

	public static function getNotifications(){
		global $DB;
		
		$notifications = $DB->get_records("report_lb_notifications");

		$arr = array();

		foreach ($notifications as $key => $notification) {
			$arr[] = $notification;
		}

		return $arr;
	}


	public static function getNotification($id){
		global $DB;

		if (!$id) {
			throw new Exception("Invalid ID!");
		}
		

		$notification = $DB->get_record("report_lb_notifications", array("id"=>$id));

		// $arr = array();

		// foreach ($notifications as $key => $notification) {
		// 	$arr[] = $notification;
		// }

		return $notification;
	}

}
