<?php

require_once('../../config.php');

require_login();

$schedule = optional_param('schedule', 'now', PARAM_TEXT);
$name = optional_param('name', '', PARAM_TEXT);

$scheduledReport = new stdClass();
$scheduledReport->userid = $USER->id;
$scheduledReport->params = $_SERVER['QUERY_STRING'];
$scheduledReport->schedule = $schedule;
$scheduledReport->name = $name;

$DB->insert_record('report_learnbook', $scheduledReport);

redirect($CFG->wwwroot . '/report/learnbook/index.php?' . $_SERVER['QUERY_STRING'] , 'The report has been scheduled and will be emailed to you shortly.');

exit;