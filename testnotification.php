<?php

require_once('../../../config.php');

require_once($CFG->dirroot . '/local/learnbook/vendor/autoload.php');
require_once($CFG->dirroot . '/local/learnbook/lib.php');
require_once($CFG->dirroot . '/local/learnbook/report/lib.php');

use Handlebars\Handlebars;

$PAGE->set_pagelayout('frametop');
$PAGE->set_url('/local/learnbook/report/testnotification.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Training Report');
$PAGE->navbar->add(get_string('report'));
$PAGE->navbar->add('Learnbook Reports');
$PAGE->navbar->add('Training Report', new moodle_url('/local/learnbook/report/index.php'));
$PAGE->navbar->add('Notifications', new moodle_url('/local/learnbook/report/testnotification.php'));

function fetchNotificationData($string) {
	$params = array();
	parse_str($string, $params);
	$daterange = explode(' - ', @$params['daterange']);
	$startdate = false;
	$enddate = false;
	if ($daterange) {
		$startdate = strtotime($daterange[0]);
		if (isset($daterange[1])) {
			$enddate = new DateTime();
			$enddate->setTimestamp(strtotime($daterange[1]));
			$enddate->modify('tomorrow');
			$enddate = $enddate->getTimestamp();
		} else {
			$enddate = new DateTime();
			$enddate->setTimestamp($startdate);
			$enddate->modify('tomorrow');
			$enddate = $enddate->getTimestamp();
		}
	}
	$cohortids = $params['cohorts'];
	$courseids = $params['courses'];
	$showAllCohorts = in_array('-1', $cohortids);
	$showAllCourses = in_array('-1', $courseids);
	if ($showAllCohorts) {
		$userids = array();
	} else {
		$userids = local_learnbook_report::fetchUsersInCohorts($cohortids, @$options['excludesuspended']);
	}
	if ($showAllCourses) {
		$courseids = array();
	}
	$options = array();
	$options['excludesuspended'] = $params['excludesuspended'];
	$options['startdate'] = $startdate;
	$options['enddate'] = $enddate;
	$options['status'] = $params['status'];
	$options['limit'] = 500000;
	$data = local_learnbook_usermodel::fetchUsers($userids, $courseids, $options, $params);
	return $data;
}

echo $OUTPUT->header();

$template = optional_param('template', false, PARAM_RAW);

// print_r($template);
?>
<a href="/local/learnbook/report/index.php"><button class="btn btn-default">Back To Report</button></a>

<?php 
if ($template) {

	$string = required_param('parameters', PARAM_RAW);

	$data = fetchNotificationData($string);

	$handlebars = new Handlebars();
	?>
	


	<?php 



	foreach ($data as $notificationdata) {

		$firstCourse = array_values($notificationdata->courses)[0];
		$firstResult = array_values($firstCourse->results)[0];

		$notificationdata->course = $firstCourse;
		$notificationdata->activity = $firstResult;

		$emailnotification = $handlebars->render($template, $notificationdata);


	}

	?>


	<?php

} else {

	




}

$params = array();
	parse_str($_SERVER['QUERY_STRING'], $params);

	$data = fetchNotificationData($_SERVER['QUERY_STRING']);

	$firstUser = array_values($data)[0];
	$firstUser = (array) $firstUser;

	?>

	<pre style="display: none;">
	<?php echo json_encode($firstUser, JSON_PRETTY_PRINT); ?>
	</pre>

	<h4>Saved Notifications</h4>

	<table class="table table-responsive">
		<thead>
			<th>Notification Name</th>
			<th>Active</th>
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tbody>
		<tr>
			<td style="min-width: 300px;"><?php echo $template ?></td>
			<td><button class="btn btn-default"><i class="fa fa-eye-slash"></i></button></td>
			<td><button class="btn btn-default"><i class="fa fa-cog"></i></button></td>
			<td><button class="btn btn-danger"><i class="fa fa-trash"></i></button></td>
		</tr>
		</tbody>

	</table>

	
<a href="/local/learnbook/report/"><button type="button" class="btn btn-primary btn-md" >
		Back to reports
	</button></a>

	<div class="modal fade" id="newnotificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	    
	    <form>
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">New Notification</h4>
	      </div>
	      

	      <div class="modal-body">

		    <div class="row">
	
		      		<div class="form-group">
		      		    
		      		    

	          			<label class="col-sm-2 control-label" for="templatename">Name of Notification</label>
	          			
	          			<div class="col-sm-10">
	    	      			<input type="hidden" name="parameters" value="<?php echo $_SERVER['QUERY_STRING']; ?>">
	    	      			<input type="text" class="form-control" id="templatename" placeholder="Name">
	          			</div>

		      		</div>
		    </div>
		    <div class="row">
		      		<div class="form-group">
		      			<label class="col-sm-2 control-label" for="ccaddress">CC Email Address:</label>

		      			<div class="col-sm-10">
			      			<input type="hidden" name="parameters" value="<?php echo $_SERVER['QUERY_STRING']; ?>">
			      			<input type="text" class="form-control" id="ccaddress" placeholder="CC Address">
						</div>

		      		</div>
		    </div>
		    <br>
			<div class="row">
					<div class="col-sm-12">
		      		<label class="control-label" for="ccaddress">Notification Text:</label>
		      		

		      		
		      			
		      			<textarea name="template">Hi {{profile.firstname}} {{profile.lastname}},
		      			
		      					You're recieving this email becuase you have not completed the following course:
		      			
		      					{{course.fullName}}
		      			
		      					You are required to complete this course to obtain your certification.
		      			</textarea>

		      		</div>
		      		
		    </div>
		    <br>
	        <div class="row">
	          		<div class="form-group">
	          			<label class="col-sm-6 control-label" for="ccaddress">Notification Interval:</label>

	          			<div class="col-sm-6">
	    	      			<input type="hidden" name="parameters" value="<?php echo $_SERVER['QUERY_STRING']; ?>">
	    	      			<select name="sendinterval">
	    	      				<option value="daily">Daily</option>
	    	      				<option value="weekly">Every 2 Days</option>
	    	      				<option value="weekly">Weekly</option>
	    	      			</select>
	    				</div>

	          		</div>
	        </div>
            <div class="row">
              		<div class="form-group">
              			<label class="col-sm-6 control-label" for="ccaddress">Maximum amount of times to send email notification:</label>

              			<div class="col-sm-6">
        	      			<input type="hidden" name="parameters" value="<?php echo $_SERVER['QUERY_STRING']; ?>">
        	      			<select name="maxnotifications">
        	      				<option value="1">Send Once</option>
        	      				<option value="2">Send Two Times</option>
        	      				<option value="3">Send Three Times</option>
        	      				<option value="3">Send Four Times</option>
        	      				<option value="0">Infinite</option>
        	      			</select>
        				</div>

              		</div>
            </div>  		




	      </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<input class="btn btn-primary" type="submit" value="Save" />
	      </div>

	    </form>


	  </div>
	</div>
<?php 
echo $OUTPUT->footer();