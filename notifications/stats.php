<?php

define('AJAX_SCRIPT', true);

require_once('../../../../config.php');

$stats = $DB->get_record_sql("	SELECT
								(SELECT SUM(value) FROM mdl_user_preferences WHERE name LIKE 'LN-TimesSent-%') as 'totalSent',
								(SELECT COUNT(userid) FROM mdl_user_preferences WHERE name LIKE 'LN-TimesSent-%') as 'totalUsers',
								(SELECT COUNT(id) FROM mdl_local_learnbook_notifications) as 'totalNotifications',
								(SELECT COUNT(id) FROM mdl_local_learnbook_notifications WHERE active = 1) as 'totalEnabledNotifications'");

echo json_encode($stats);