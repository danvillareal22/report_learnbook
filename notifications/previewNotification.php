<?php

define('AJAX_SCRIPT', true);

require_once('../../../../config.php');

require_once($CFG->dirroot . '/local/learnbook/vendor/autoload.php');
require_once($CFG->dirroot . '/local/learnbook/lib.php');
require_once($CFG->dirroot . '/local/learnbook/report/lib.php');

use Handlebars\Handlebars;

$params = json_decode(trim(file_get_contents('php://input'), "'"), true);

// $string = required_param('parameters', PARAM_RAW);

$string = $params["params"];
// eg $string = 'cohorts%5B%5D=-1&courses%5B%5D=-1&searchterm=&daterange=&status=-1&excludesuspended=0&excludesuspended=1&includeCourseGrades=0&queryBuilderState=%7B"condition"%3A"AND"%2C"rules"%3A%5B%5D%7D&queryBuilderQuery=%7B"sql"%3A""%2C"params"%3A%5B%5D%7D';

$data = local_learnbook_report::fetchNotificationData($string, 200);
$completeData = local_learnbook_report::fetchNotificationData($string);
// $template = required_param('template', PARAM_RAW);
$template = $params["body"];


$output = new stdClass();
$output->totalUsers = count($completeData);
$output->preview = array();
$totalUsers = count($completeData);

$handlebars = new Handlebars();
foreach ($data as $akey => $notificationdata) {
	$firstCourse = array_values($notificationdata->courses)[0];
	$firstResult = array_values($firstCourse->results)[0];
	$notificationdata->course = $firstCourse;
	$notificationdata->activity = $firstResult;
	$emailnotification = $handlebars->render($template, $notificationdata);
	$emailnotification = $emailnotification;

	$object = new stdClass();
	$object->userid = $akey;
	$object->profile = $notificationdata->profile;
	$object->notification = $emailnotification;
	$output->preview[$akey] = $object;
}

echo json_encode($output);
