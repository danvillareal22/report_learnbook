<?php

require_once('../../../config.php');

require_once($CFG->dirroot . '/report/learnbook/vendor/autoload.php');
require_once($CFG->dirroot . '/report/learnbook/lib.php');
require_once($CFG->dirroot . '/report/learnbook/lib.php');

use Handlebars\Handlebars;

$PAGE->set_pagelayout('frametop');
$PAGE->set_url('/report/learnbook/notifications/index.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Training Report');
$PAGE->navbar->add(get_string('report'));
$PAGE->navbar->add('Learnbook Reports');
$PAGE->navbar->add('Training Report', new moodle_url('/report/learnbook/index.php'));
$PAGE->navbar->add('Notifications', new moodle_url('/report/learnbook/testnotification.php'));

$PAGE->requires->js('/theme/learnbook/bower_components/angular/angular.js', true);
$PAGE->requires->js('/theme/learnbook/bower_components/angular-animate/angular-animate.js', true);
$PAGE->requires->js('/theme/learnbook/bower_components/angular-aria/angular-aria.js', true);
$PAGE->requires->js('/theme/learnbook/bower_components/angular-material/angular-material.js', true);

$PAGE->requires->js('/report/learnbook/notifications/controllers/notifications.js', true);
$PAGE->requires->js('/report/learnbook/notifications/services/reportService.js', true);



// $handlebars = new Handlebars();

// $notifications = local_learnbook_report::getNotifications();

echo $OUTPUT->header();
// print_r($notifications);

ob_start();

?>
<a href="/report/learnbook/index.php#tab_notifications"><button class="btn btn-primary">Back To Report</button></a>





	<h4>Saved Notifications</h4>

<div ng-controller="manageNotifications">

	<table class="table table-responsive" >
		<thead>
			<th>Notification Name</th>
			<th>Active</th>
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tbody>
		<tr ng-repeat="notification in notifications" ng-controller="notificationController">
			<td style="min-width: 300px;">{{notification.name}}</td>
			<td><button ng-click="toggleActive()" class="btn btn-default"><i ng-class="{ 'fa-eye-slash'   : notification.active == 0, 'fa-eye'   : notification.active == 1}" class="fa"></i></button></td>
			<td><button ng-click="editNotification()" class="btn btn-default"><i class="fa fa-cog"></i></button></td>
			<td><button ng-click="deleteNotification()" class="btn btn-danger"><i class="fa fa-trash"></i></button></td>
		</tr>
		</tbody>
	
	</table>
</div>

	
<a href="/report/learnbook/index.php#tab_notifications"><button type="button" class="btn btn-primary btn-md" >
		Back to Report
	</button></a>

<?php 

$table = ob_get_clean();

// $emailnotification = $handlebars->render($template, $notifications);

echo $table;

echo $OUTPUT->footer();