learnbookApp.factory('reportService', ['$http', '$rootScope', function($http, $rootScope) {

	return {

		getNotifications: function() {
		return $http.get('/local/learnbook/report/notifications/getnotifications.php').then(function(response) {
		    var object = response.data;

		    var arr = [];

		    angular.forEach(response.data, function(obj) {
		    	obj.startdateobject = new Date(obj.startdate * 1000);
		    	obj.enddateobject = new Date(obj.enddate * 1000);
		    	obj.interval = obj.msgint;

		    	arr.push(obj);



		    });

		    $rootScope.$broadcast('getNotifications',object);
		    return object;
		  })
		},

		getNotification: function() {
		return $http.get('/local/learnbook/report/notifications/getnotification.php').then(function(response) {
		    var object = response.data;

		    $rootScope.$broadcast('getNotification',object);
		    return object;
		  })
		},

		getVariables: function() {
			var variables = [
			{
			  name : "{{profile.username}}"
			},
			{
			  name : "{{profile.firstname}}"
			},
			{
			  name : "{{profile.lastname}}"
			},
			{
			  name : "{{profile.email}}"
			},
			{
			  name : "{{course.fullName}}"
			},
			{
			  name : "{{course.shortName}}"
			},
			{
			  name : "{{site.wwwroot}}"
			},
			{
			  name : "{{site.fullName}}"
			},
			{
			  name : "{{site.shortName}}"
			},
			{
			  name : "{{site.support_email}}"
			},
			{
			  name : "{{site.support_name}}"
			}
			];

			return variables;
		},


		getStats: function() {
			return $http.get('/local/learnbook/report/notifications/stats.php').then(function(response) {
			    var object = response.data;

			    $rootScope.$broadcast('getStats',object);
			    return object;
			  })
		},

		saveNotification: function($params) {

		  return $http({
		    url: '/local/learnbook/report/notifications/savenotification.php',
		    method: "POST",
		    data: $params
		  })
		    .then(function(response) {
		    	return response.data;

		      $rootScope.$broadcast('saveNotification',response.data);
		    });
		},


		previewNotifications: function($params) {

		  return $http({
		    url: '/local/learnbook/report/notifications/previewNotification.php',
		    method: "POST",
		    data: $params
		  })
		    .success(function(data) {
		    	console.log(data);
		      
		      $rootScope.$broadcast('saveForm',data);
		    });
		},
		
  	};
}]);