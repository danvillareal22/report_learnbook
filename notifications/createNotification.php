<?php

require_once('../../../config.php');

require_once($CFG->dirroot . '/report/learnbook/vendor/autoload.php');
require_once($CFG->dirroot . '/report/learnbook/lib.php');
require_once($CFG->dirroot . '/report/learnbook/lib.php');

use Handlebars\Handlebars;

$PAGE->set_pagelayout('frametop');
$PAGE->set_url('/report/learnbook/notifications/index.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Training Report');
$PAGE->navbar->add(get_string('report'));
$PAGE->navbar->add('Learnbook Reports');
$PAGE->navbar->add('Training Report', new moodle_url('/report/learnbook/index.php'));
$PAGE->navbar->add('Notifications', new moodle_url('/report/learnbook/testnotification.php'));

$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url('/report/learnbook/js/jQuery.extendext.min.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/js/doT.min.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/js/notification.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/lib/select2/dist/js/select2.js'), true);
$PAGE->requires->css(new moodle_url('/report/learnbook/lib/select2/dist/css/select2.css'));
$PAGE->requires->css('/report/learnbook/lib/daterangepicker/daterangepicker-bs3.css');
$PAGE->requires->js('/report/learnbook/lib/daterangepicker/moment.min.js', true);
$PAGE->requires->js('/report/learnbook/lib/daterangepicker/daterangepicker.js', true);

$PAGE->requires->css(new moodle_url('/report/learnbook/css/report.css'));
$PAGE->requires->css('/report/learnbook/css/bootstrap-datepicker.min.css');
$PAGE->requires->css('/report/learnbook/css/query-builder.default.min.css');
$PAGE->requires->js(new moodle_url('/report/learnbook/js/query-builder.min.js'), true);
$PAGE->requires->js(new moodle_url('/report/learnbook/js/bootstrap-datepicker.min.js'), true);
require_once($CFG->dirroot . '/report/learnbook/moodleQueryBuilder.php');
echo moodleQueryBuilder::renderJavascriptVariables();
echo $OUTPUT->header();

?>
<div class="container">
	<div class="row">
		<div class="span12 col-md-12">
			<form class="form-horizontal" id="learnbook_report">
				<legend><i class="fa fa-filter"></i>&nbsp;Notification Audience</legend>
				<div class="form-group">
					<label class="col-sm-1 control-label" for="notificationtemplate">Event</label>
					<div class="col-sm-11">
						<select name="notificationtemplate">
							<option value="on_enrolment">On Enrolment</option>
							<option value="incompleteOneMonthAfterAccountCreation">Incomplete after 1 Month after Account Created</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" for="cohorts">Cohorts</label>
					<div class="col-sm-11">
						<?php echo report_learnbook_report::form_cohort_multiselect(); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" for="cohorts">Courses</label>
					<div class="col-sm-11">
						<?php echo report_learnbook_report::form_course_multiselect(); ?>
					</div>
				</div>
				<?php
				$config = get_config('report_learnbook');
				$config->enableactivitylevelfiltering = false;
				if ($config->enableactivitylevelfiltering) { ?>
				<div class="form-group">
					<label class="col-sm-1 control-label" for="cohorts">Activities</label>
					<?php
					$selectedactivities = optional_param_array('report_learnbook_report_activities', array(), PARAM_RAW);
					?>
					<div class="col-sm-11">
						<select multiple id="report_learnbook_report_activities" name="report_learnbook_report_activities[]">
							<?php
							if (!empty($selectedactivities)) {
								$data = report_learnbook_report::constructGradeItemSelector();
								foreach ($selectedactivities as $itemid) {
									echo '<option selected value="' . $itemid . '">' . $data[$itemid] . '</option>';
								}
							}
							?>	
						</select>
					</div>
				</div>
				<?php }
				$searchterm = optional_param('searchterm', '', PARAM_RAW);
				?>
				<input type="hidden" name="queryBuilderState" value='<?php echo optional_param('queryBuilderState', '{"condition":"AND","rules":[]}', PARAM_RAW); ?>' /> 
				<input type="hidden" name="queryBuilderQuery" />
				<input type="hidden" id="report_learnbook_search_type" value="" name="searchterm" />
				<input id="learnbook_report_excludesuspended" type="hidden" value="1" name="excludesuspended">
				<input type="hidden" value="1" name="includeCourseGrades" />
				<input id="learnbook_report_status" type="hidden" value="-1" name="status" />
				<div class="form-group">
					<label class="col-sm-1 control-label" for="startdate">Date Range</label>
					<div class="col-sm-3">
						<input type="text" name="daterange" class="date" placeholder="Select a Date Range" />
						<!-- <p class="help-block">Effective start date for this email notification.</p> -->
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" for="maxsend">Repeat</label>
					<div class="col-sm-3">
						<select name="maxsend">
							<option value="1">Send Once</option>
							<option value="2">Send Two Times</option>
							<option value="3">Send Three Times</option>
							<option value="4">Send Four Times</option>
							<option value="5">Send Five Times</option>
							<option value="6">Send Six Times</option>
							<option value="7">Send Seven Times</option>
							<option value="8">Send Eight Times</option>
							<option value="9">Send Nine Times</option>
							<option value="10">Send Ten Times</option>
							<option value="-1">Until End Date</option>
						</select>
						<!-- <p class="help-block">Maximum times this notification will be sent to each recipient.</p> -->
					</div>
					<label class="col-sm-1 control-label" for="sendinterval">Interval</label>
					<div class="col-sm-3">
						<select name="sendinterval" required="true">
							<option value="first day of this month">First Day of Month</option>
							<option value="last day of this month">Last Day of Month</option>
							<option value="monday">Every Monday</option>
							<option value="tuesday">Every Tuesday</option>
							<option value="wednesday">Every Wednesday</option>
							<option value="thursday">Every Thursday</option>
							<option value="friday">Every Friday</option>
							<option value="saturday">Every Saturday</option>
							<option value="sunday">Every Sunday</option>
							<option value="today">Everyday</option>
						</select>
						<!-- <p class="help-block">The scheduled interval to send this notification.</p> -->
					</div>
					<div class="col-sm-4">
						<button class="btn btn-primary pull-right">Save Notification</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<?php

echo $OUTPUT->footer();