var learnbookApp = angular.module('learnbookApp', ['ngAnimate','ngMaterial']);

// learnbookApp.config(function($mdDateLocaleProvider) {
// 	$mdDateLocaleProvider.formatDate = function(date) {
// 	   return moment(date).format('DD-MM-YYYY');
// 	};
// });

learnbookApp.controller('manageNotifications', ['$scope', '$window', 'reportService','$mdDialog',
    function($scope, $window, reportService,$mdDialog) {

 	// console.log("response");

    	reportService.getNotifications().then( function(response){
    		$scope.notifications = response;

    		// console.log(response);

    	});

}]);

learnbookApp.controller('notificationReport', ['$scope', '$window', 'reportService','$mdDialog',
    function($scope, $window, reportService,$mdDialog) {



    	reportService.getStats().then( function(response){
    		
    		

            if (response.totalenablednotifications == null) {
                $scope.totalenablednotifications = 0;
            }else{
                $scope.totalenablednotifications = response.totalenablednotifications;
            }

            if (response.totalsent == null) {
                $scope.sentnotifications = 0;
            }else{
                $scope.sentnotifications = response.totalsent;
            }


    	});

        t = [
            'Dear {{profile.firstname}} {{profile.lastname}},',
            '',
            'This is an example email notification which has been fully configured to notify you about your progress in {{site.fullName}}.',
            '',
            'This particular notification was triggered because you either completed or did not complete the following course:',
            '',
            '{{course.fullName}} ({{course.shortName}}',
            '',
            'Please login to your account at: {{site.wwwroot}} using your username ({{profile.username}}) and password.',
            '',
            'If you have forgotten your password you can reset it at {{site.wwwroot}}/login/forgot_password.php.',
            '',
            'Otherwise you can contact our help desk by sending an email to: {{site.support_email}}',
            '',
            'Kind Regards,',
            '',
            '{{site.support_name}}'
        ].join("\n");

    	$scope.selectedTemplate = 
        {
            templateid: 1,
            name: "Completion Template",
            active: true,
            body: t,
        }

    	$scope.templates = [

    		{
    			templateid: 1,
    			name: "Completion Template",
                active: true,
    			body: t,
    		},
    		{
    			templateid: 2,
    			name: "Incompletion Template",
                active: true,
    			body: t,
                subject: "Incompletion Template"
    		}   		

    	];

    	


    	$scope.addNotification = function(params){

    		$mdDialog.show({
    		   clickOutsideToClose: true,
    		   scope: $scope,        // use parent scope in template
    		   preserveScope: true,  // do not forget this if use parent scope
    		   templateUrl: '/local/learnbook/report/notifications/views/editnotificationdialog.html',
    		   controller: function AddClientResultDialog($scope, $window, $mdDialog,reportService) {
    		   	    $scope.title = "Add Notification";

    		   		$scope.variables = reportService.getVariables();



                    $scope.editing = false;

    		   		$scope.notification = $scope.selectedTemplate;

   
                    var loc = window.location.pathname;
                    var dir = loc.substring(0, loc.lastIndexOf('/'));

                    string = $('#learnbook_report').serialize();
                    // string = string + "&name=" + $('#learnbook_report_savedreports_reportname').val() + "&schedule=" + $('#learnbook_report_savedreports_reportschedule').val();
                    $scope.notification.params = string;

                    $scope.notification.active = true;
                    $scope.notification.update = 0;

    		   		$scope.notification.maxsend = "1";


                    $scope.saveStartDate = function() {
                        $scope.notification.startdate = $scope.notification.startdateobject.toDateString();
                    }  

                    $scope.saveEndDate = function() {
                       $scope.notification.enddate = $scope.notification.enddateobject.toDateString();
                    }  


    		   		$scope.closeDialog = function() {
    		   		   $mdDialog.hide();
    		   		}


    		   		$scope.addTemplate = function(template) {


    		   		  function insertAtCursor(myField, myValue) {
    		   		      //IE support
    		   		      if (document.selection) {
    		   		          myField.focus();
    		   		          sel = document.selection.createRange();
    		   		          sel.text = myValue;
    		   		      }
    		   		      //MOZILLA and others
    		   		      else if (myField.selectionStart || myField.selectionStart == '0') {
    		   		          var startPos = myField.selectionStart;
    		   		          var endPos = myField.selectionEnd;
    		   		          myField.value = myField.value.substring(0, startPos)
    		   		              + myValue
    		   		              + myField.value.substring(endPos, myField.value.length);
    		   		      } else {
    		   		          myField.value += myValue;
    		   		      }

                          $scope.notification.body = myField.value;
    		   		  }

    		   		  insertAtCursor(document.getElementById("welcomeEmailText"), template);

    		   		}


    		   		$scope.previewNotification = function() {


					$scope.template = 	'<md-dialog class="info">' +
										'	<md-toolbar>' +
										'	  <div class="md-toolbar-tools">' +
										'	    <h2>{{title}}</h2>' +
										'	    <span flex></span>' +
										'	    <md-button class="md-icon-button" ng-click="closeDialog()">' +
										'	    	<i class="fa fa-times"></i>' +
										'	    </md-button>' +
										'	  </div>' +
										'	</md-toolbar>' +

			                            '  <md-dialog-content>' +
			                            '     {{content}}' +
			                            '  </md-dialog-content>' +
			                            '  <md-dialog-actions> ' +
			                            '    <button class="btn btn-warning pull-right" ng-click="closeDialog()" style="margin-right:20px; margin-bottom: 20px;"> ' +
			                            '      Close ' +
			                            '    </button> ' +
			                            '  </md-dialog-actions> ' +
			                              '</md-dialog>';



    		   			reportService.saveNotification($scope.notification).then( function(response){

    		   				if (response.error) {


                                $mdDialog.hide();

                                $scope.title = "Notification Error";

                                $scope.content = "Error";


                               $mdDialog.show({
                                  clickOutsideToClose: true,
                                  scope: $scope,        
                                  preserveScope: true,
                                  template : $scope.template,           

                                  controller: function DialogController($scope, $mdDialog) {
                                     $scope.closeDialog = function() {
                                        $mdDialog.hide();
                                     }
                                  }
                               });


    		   					
    		   				}else{
	                              $mdDialog.hide();   

                                $scope.title = "Notification Saved";

                                $scope.content = "This notification has been successfully saved.";


                               $mdDialog.show({
                                  clickOutsideToClose: true,
                                  scope: $scope,        
                                  preserveScope: true,
                                  template : $scope.template,           

                                  controller: function DialogController($scope, $window, $mdDialog) {
                                     $scope.closeDialog = function() {
                                        $mdDialog.hide();

                                        // $window.location.href = '/local/learnbook/report/notifications/index.php';
                                     }
                                  }
                               });

    		   				}

                            
                                reportService.getStats().then( function(response){
                                    
                                    

                                    if (response.totalenablednotifications == null) {
                                        $scope.totalenablednotifications = 0;
                                    }else{
                                        $scope.totalenablednotifications = response.totalenablednotifications;
                                    }

                                    if (response.totalsent == null) {
                                        $scope.sentnotifications = 0;
                                    }else{
                                        $scope.sentnotifications = response.totalsent;
                                    }


                                });


    		   		});
    		   		

    		   	    $scope.closeDialog = function() {
    		   	       $mdDialog.hide();
    		   	     }



    		   };
    		}

    	});


    	

    	}
}]);




learnbookApp.controller('notificationController', ['$scope', '$window', 'reportService','$mdDialog',
    function($scope, $window, reportService,$mdDialog) {

	$scope.toggleActive = function () {
        console.log($scope.notification.active);
        if ($scope.notification.active == 0) {
            $scope.notification.active = 1;
        }else{
            $scope.notification.active = 0;
        }

		//$scope.notification.active = !$scope.notification.active;
		$scope.notification.update = 1;

		reportService.saveNotification($scope.notification);
	}


    $scope.deleteNotification = function () {

    	var index = $scope.notifications.indexOf($scope.notification);
    	$scope.notifications.splice(index, 1);   
    	// save this here
    	$scope.notification.update = 0;
    	$scope.notification.delete = 1;
    	reportService.saveNotification($scope.notification);
    }

    $scope.editNotification = function () {

    	$mdDialog.show({
    	   clickOutsideToClose: true,
    	   scope: $scope,        // use parent scope in template
    	   preserveScope: true,  // do not forget this if use parent scope
    	   templateUrl: '/local/learnbook/report/notifications/views/editnotificationdialog.html',
    	   controller: function AddClientResultDialog($scope, $window, $mdDialog,reportService) {
    	    
    	   	$scope.variables = reportService.getVariables();

            $scope.title = "Edit Notification";
            $scope.editing = true;
    	   	// reportService.getNotification($scope.notification.id).then( function(response){
    	   	// 	$scope.notification = response;
    	   	// });



    	   	$scope.addTemplate = function(template) {


    	   	  function insertAtCursor(myField, myValue) {
    	   	      //IE support
    	   	      if (document.selection) {
    	   	          myField.focus();
    	   	          sel = document.selection.createRange();
    	   	          sel.text = myValue;
    	   	      }
    	   	      //MOZILLA and others
    	   	      else if (myField.selectionStart || myField.selectionStart == '0') {
    	   	          var startPos = myField.selectionStart;
    	   	          var endPos = myField.selectionEnd;
    	   	          myField.value = myField.value.substring(0, startPos)
    	   	              + myValue
    	   	              + myField.value.substring(endPos, myField.value.length);
    	   	      } else {
    	   	          myField.value += myValue;
    	   	      }
    	   	  }

    	   	  insertAtCursor(document.getElementById("welcomeEmailText"), template);
    	   	}

    	     $scope.closeDialog = function() {
    	       $mdDialog.hide();
    	     }


    	         		   		$scope.saveNotification = function() {



    	         		   		$scope.notification.update = 1;

    	     					$scope.template = 	'<md-dialog class="info">' +
    	     										'	<md-toolbar>' +
    	     										'	  <div class="md-toolbar-tools">' +
    	     										'	    <h2>{{title}}</h2>' +
    	     										'	    <span flex></span>' +
    	     										'	    <md-button class="md-icon-button" ng-click="closeDialog()">' +
    	     										'	    	<i class="fa fa-times"></i>' +
    	     										'	    </md-button>' +
    	     										'	  </div>' +
    	     										'	</md-toolbar>' +

    	     			                            '  <md-dialog-content>' +
    	     			                            '     {{content}}' +
    	     			                            '  </md-dialog-content>' +
    	     			                            '  <md-dialog-actions> ' +
    	     			                            '    <button class="btn btn-warning pull-right" ng-click="closeDialog()" style="margin-right:20px; margin-bottom: 20px;"> ' +
    	     			                            '      Close ' +
    	     			                            '    </button> ' +
    	     			                            '  </md-dialog-actions> ' +
    	     			                              '</md-dialog>';



    	         		   			reportService.saveNotification($scope.notification).then( function(response){

    	         		   				if (!response.error) {
    	         		   					$mdDialog.hide();	

    	         		   					$scope.title = "Notification Created";

    	         		   					$scope.content = "Success";


    	     				               $mdDialog.show({
    	     				                  clickOutsideToClose: true,
    	     				                  scope: $scope,        
    	     				                  preserveScope: true,
    	     				                  template : $scope.template,           

    	     				                  controller: function DialogController($scope, $mdDialog) {
    	     				                     $scope.closeDialog = function() {
    	     				                        $mdDialog.hide();
    	     				                     }
    	     				                  }
    	     				               });


    	         		   					
    	         		   				}else{
    	         		   					$mdDialog.hide();

    	         		   					$scope.title = "Notification Error";

    	         		   					$scope.content = "Error";


    	     				               $mdDialog.show({
    	     				                  clickOutsideToClose: true,
    	     				                  scope: $scope,        
    	     				                  preserveScope: true,
    	     				                  template : $scope.template,           

    	     				                  controller: function DialogController($scope, $mdDialog) {
    	     				                     $scope.closeDialog = function() {
    	     				                        $mdDialog.hide();
    	     				                     }
    	     				                  }
    	     				               });

    	         		   			}


    	         		   		});
    	         		   		

    	         		   	    $scope.closeDialog = function() {
    	         		   	       $mdDialog.hide();
    	         		   	     }



    	         		   };



    	   }

    	});

    }





}]);




