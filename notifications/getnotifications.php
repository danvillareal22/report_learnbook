<?php
define('AJAX_SCRIPT', true);

require_once('../../../../config.php');

require_once($CFG->dirroot . '/local/learnbook/vendor/autoload.php');
require_once($CFG->dirroot . '/local/learnbook/lib.php');
require_once($CFG->dirroot . '/local/learnbook/report/lib.php');


$notifications = local_learnbook_report::getNotifications();

print_r(json_encode($notifications));

?>