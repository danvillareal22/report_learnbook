<?php

define('AJAX_SCRIPT', true);

require_once('../../../../config.php');

// require_once($CFG->dirroot . '/local/learnbook/vendor/autoload.php');
// require_once($CFG->dirroot . '/local/learnbook/lib.php');
// require_once($CFG->dirroot . '/local/learnbook/report/lib.php');

$params = json_decode(trim(file_get_contents('php://input'), "'"), true);

// $name = required_param('name', PARAM_TEXT);
// $cc = required_param('cc', PARAM_TEXT);
// $body = required_param('body', PARAM_TEXT);
// $maxsend = required_param('maxsend', PARAM_TEXT);
// $active = required_param('active', PARAM_TEXT);
// $params = required_param('params', PARAM_TEXT);

$notification = new stdClass;
$notification->name = $params["name"];
$notification->cc = $params["cc"];
$notification->body = $params["body"];
$notification->maxsend = $params["maxsend"];
$notification->active = $params["active"];
$notification->params = $params["params"];

$notification->subject = $params["subject"];


if (( is_numeric($params['startdate']) && (int)$params['startdate'] == $params['startdate'] )) {
	# do nothing
}else{
	$startdate = new DateTime($params['startdate']);
	$startdate->setTimeZone(new DateTimeZone('Australia/Sydney')); // Fix this in future
	$startdate->modify('midnight');
	$startdate = $startdate->format('U');
}


if (( is_numeric($params['enddate']) && (int)$params['enddate'] == $params['enddate'] )) {
	# do nothing
}else{

	$enddate = new DateTime($params['enddate']);
	$enddate->setTimeZone(new DateTimeZone('Australia/Sydney')); // Fix this in future
	$enddate->modify('midnight');
	$enddate = $enddate->format('U');
}


$notification->startdate = $startdate;
$notification->enddate = $enddate;



//i dont know why this isnt saving - disabling for now
$notification->msgint = $params["interval"];



if ($params["update"]) {
	$notification->id = $params["id"];

	$DB->update_record("local_learnbook_notifications", $notification);
}elseif($params["delete"]){

	$DB->delete_records("local_learnbook_notifications",  array("id"=>$params["id"]));
}else {

	$success = $DB->insert_record_raw("local_learnbook_notifications", $notification);
}


echo json_encode($notification);




?>