<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php'); // global moodle config file.

require_once($CFG->dirroot . '/report/learnbook/lib.php');

mtrace('Processing Notification Schedule');

report_learnbook_report::notifyReports();

mtrace('Finished Processing Notification Schedule');