<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php'); // global moodle config file.

require_once($CFG->dirroot . '/report/learnbook/lib.php');
// require_once($CFG->dirroot . '/local/learnbook/report/lib.php');

mtrace('Processing End User Notifications');

report_learnbook_report::processEndUserNotifications();

mtrace('Finished Processing End User Notifications');