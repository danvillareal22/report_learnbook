<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php'); // global moodle config file.

require_once($CFG->dirroot . '/report/learnbook/lib.php');

mtrace('Processing Schedule');

report_learnbook_report::processSchedule();

mtrace('Finished Processing Schedule');